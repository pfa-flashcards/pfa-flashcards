const express = require("express");
const authJWT = require("../middleware/auth");
const router = express.Router();

const {
  createCard,
  getCard,
  updateCard,
  deleteCard,
} = require("../controllers/cards");

/**
 * @route POST api/cards
 * @desc  Verify user authentication and create card in deck
 * @access Private
 */

router.post("/", authJWT, createCard);

/**
 * @route GET api/cards/$card_id
 * @desc  Verify user authentication and get a card
 * @access Private
 */

router.get("/:card_id", authJWT, getCard);

/**
 * @route PUT api/cards/$card_id
 * @desc  Verify user authentication and modify a card
 * @access Private
 */

router.put("/:card_id", authJWT, updateCard);

/**
 * @route DELETE api/cards/$card_id
 * @desc  Verify user authentication and delete a card
 * @access Private
 */

router.delete("/:card_id", authJWT, deleteCard);

module.exports = router;
