const express = require("express");
const authJWT = require("../middleware/auth");
const router = express.Router();
const {
  createDeck,
  getUserDecks,
  getDeck,
  deleteDeck,
  getDeckCards,
  getDeckCardsSorted,
  searchDeckByPattern,
  linkUserToDeck,
  setDeckPrivacy,
} = require("../controllers/decks");

// @route POST api/decks
// @desc  Verify user authentication and create deck
// @access Private
router.post("/", authJWT, createDeck);

// @route GET api/decks
// @desc  Verify user authentication and get the decks of the user
// @access Private
router.get("/", authJWT, getUserDecks);

// @route GET api/decks/<deckId>
// @desc  Get information about a deck
// @access Private
router.get("/:deckId", authJWT, getDeck);

// @route DELETE api/decks/<deckId>
// @desc  delete a deck
// @access Private
router.delete("/:deckId", authJWT, deleteDeck);

// @route GET api/decks/<deckId>/cards
// @desc  Verify user authentication and get the cards of the deck
// @access Private
router.get("/:deckId/cards", authJWT, getDeckCards);

// @route GET api/decks/<deckId>/cards/sorted
// @desc  Verify user authentication and get the cards of the deck sorted
// @access Private
router.get("/:deckId/cards/sorted", authJWT, getDeckCardsSorted);

// @route GET api/decks/search/<pattern>
// @desc  Verify user authentication and get the deck with a particular pattern
// @access Private
router.get("/search/:pattern", authJWT, searchDeckByPattern);

// @route PUT api/decks/<deckId>/link
// @desc  Put a user in the autorized field of the deck
// @access Private
router.put("/:deckId/link", authJWT, linkUserToDeck);

// @route PUT api/decks/<deckId>/privacy/<privacy>
// @desc  Verify user authentication and set the privacy of the deck
// @access Private
router.put("/:deckId/privacy/:privacy", authJWT, setDeckPrivacy);

module.exports = router;
