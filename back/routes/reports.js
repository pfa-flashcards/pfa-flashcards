const express = require("express");
const authJWT = require("../middleware/auth");
const router = express.Router();
const {
  createReport,
  updateReport,
  getReportsByDeckId,
  getLastReport,
  deleteReportsByDeckId,
  getReportsByDeckIdForAuthor,
} = require("../controllers/reports");

/**
 * @route POST api/reports
 * @desc  Verify user authentification and create a report
 * @access Private
 */

router.post("/", authJWT, createReport);

/**
 * @route PUT api/reports/<reportId>
 * @desc  Verify user authentification, update a report and return it
 * @access Private
 */

router.put("/:reportId", authJWT, updateReport);

/**
 * @route GET api/reports/deck/<deckId>
 * @desc  Verify user authentification and return reports for a deck
 * @access Private
 */

router.get("/deck/:deckId", authJWT, getReportsByDeckId);

/**
 * @route GET api/reports/<deckId>/last
 * @desc  Verify user authentification and return last report for a deck
 * @access Private
 */

router.get("/deck/:deckId/last", authJWT, getLastReport);

/**
 * @route GET api/reports/<deckId>/author
 * @desc  Verify user authentification and return reports of all users
 * @access Private
 */

router.get("/deck/:deckId/author", authJWT, getReportsByDeckIdForAuthor);

/**
 * @route DELETE api/reports
 * @desc  Verify user authentication and delete user reports for a given deck
 * @access Private
 */

router.delete("/", authJWT, deleteReportsByDeckId);

module.exports = router;
