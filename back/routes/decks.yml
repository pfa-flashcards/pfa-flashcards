components:
  schemas:
    Deck:
      type: object
      properties:
        name:
          type: string
        _id:
          type: string
      required: [_id, name]
    DeckStats:
      type: object
      properties:
        user:
          type: object
          properties:
            firstname:
              type: string
            lastname:
              type: string
        deck:
          type: object
          properties:
            name:
              type: string
            _id:
              type: string
            owner:
              type: string
            nbCards:
              type: number
      required: [user, deck]
    DeckStatsArray:
      type: array
      items:
        $ref: "#/components/schemas/DeckStats"

/decks:
  post:
    description: Create a new Deck
    tags: [decks]
    requestBody:
      content:
        application/json:
          schema:
            type: object
            properties:
              name:
                type: string
    responses:
      200:
        description: The created Deck
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Deck"
      500:
        description: Server error

  get:
    description: Return the Decks owned by the authenticated user
    tags: [decks]
    responses:
      200:
        description: A list of Decks
        content:
          application/json:
            schema:
              type: array
              items:
                $ref: "#/components/schemas/Deck"
      500:
        description: Server error

/decks/{deckId}:
  get:
    description: Get information about a Deck
    tags: [decks]
    parameters:
      - in: path
        name: deckId
        schema:
          type: string
        required: true
    responses:
      200:
        description: A Deck
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Deck"
      500:
        description: Server error

  delete:
    description: Delete a deck
    tags: [decks]
    parameters:
      - in: path
        name: deckId
        schema:
          type: string
        required: true
    responses:
      200:
        description: Deck deleted
        content:
          application/json: {}
      400:
        description: Not the owner
      500:
        description: Server error

/decks/{deckId}/cards:
  get:
    description: Get the cards from a Deck
    tags: [decks]
    parameters:
      - in: path
        name: deckId
        schema:
          type: string
        required: true
    responses:
      200:
        description: A list of Cards
        content:
          application/json:
            schema:
              type: array
              items:
                $ref: "#/components/schemas/Card"
      500:
        description: Server error

/decks/{deckId}/cards/sorted:
  get:
    description: Get the cards sorted from a Deck
    tags: [decks]
    parameters:
      - in: path
        name: deckId
        schema:
          type: string
        required: true
    responses:
      200:
        description: A list of Cards
        content:
          application/json:
            schema:
              type: array
              items:
                $ref: "#/components/schemas/Card"
      500:
        description: Server error

/decks/{deckId}/privacy/{privacy}:
  put:
    description: Update the privacy of a Deck
    tags: [decks]
    parameters:
      - in: path
        name: deckId
        schema:
          type: string
        required: true
      - in: path
        name: privacy
        schema:
          type: string
        required: true
    responses:
      200:
        description: The deck was updated sucessfully
      500:
        description: Server error

/decks/search/{pattern}:
  get:
    description: Return the Decks corresponding to the pattern
    tags: [decks]
    parameters:
      - in: path
        name: pattern
        schema:
          type: string
        required: true
    responses:
      200:
        description: A deck or a list of deck
        content:
          application/json:
            schema:
              type: array
              items:
                $ref: "#/components/schemas/DeckStatsArray"
      400:
        description: No matching deck found
      500:
        description: Server error

/decks/{deckId}/link:
  put:
    description: Put a user in the autorized field of the deck
    tags: [decks]
    parameters:
      - in: path
        name: deckId
        schema:
          type: string
        required: true
    responses:
      200:
        description: The Deck
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Deck"
      500:
        description: Server error
