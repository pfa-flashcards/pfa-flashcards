const express = require("express");
const authJWT = require("../middleware/auth");
const router = express.Router();
const {
  createEval,
  getEval,
  getLastEval,
  deleteEvalByCardId,
} = require("../controllers/evals");

/**
 * @route POST api/evals
 * @desc  Verify user authentification and create evaluation and deletes oldest evaluation when their number is greater than the limit set
 * @access Private
 */

router.post("/", authJWT, createEval);

/**
 * @route GET api/evals/$card_id
 * @desc Verify user authentification and return last eval
 * @access Private
 */

router.get("/:cardId", authJWT, getEval);

/**
 * @route GET api/evals/$card_id/last
 * @desc Verify user authentification and return last eval
 * @access Private
 */

router.get("/:cardId/last", authJWT, getLastEval);

/**
 * @route DELETE api/evals/$card_id
 * @desc Verify user authentification and delete evals
 * @access Private
 */

router.delete("/:cardId", authJWT, deleteEvalByCardId);

module.exports = router;
