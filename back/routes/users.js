const express = require("express");
const router = express.Router();

const authJWT = require("../middleware/auth");
const { signUp, getProfile } = require("../controllers/users");

// @route POST api/users
// @desc  Register user
// @access Public
router.post("/", signUp);

// @route GET api/users/me
// @desc  Get current users profile
// @access Private
router.get("/me", authJWT, getProfile);

module.exports = router;
