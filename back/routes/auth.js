const express = require("express");
const router = express.Router();

const { signIn } = require("../controllers/users");

// @route POST api/auth
// @desc  Authenticate user & get token
// @access Public
router.post("/", signIn);

module.exports = router;
