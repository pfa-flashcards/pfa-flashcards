const express = require("express");
const app = express();
app.use(express.json());

const { openApiValidator } = require("../openapi");
app.use(openApiValidator);

const route = "/api/users";
app.use(route, require("../routes/users"));

let request = require("supertest");
request = request(app);

const User = require("../models/User");
jest.mock("../models/User");
const bcrypt = require("bcryptjs");
jest.mock("bcryptjs");
const jwt = require("jsonwebtoken");
jest.mock("jsonwebtoken");

describe("registration route", () => {
  let user;

  beforeEach(() => {
    user = {
      firstname: "firstname",
      lastname: "lastname",
      email: "register@gmail.com",
      password: "password",
    };
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test("should reject empty request", async () => {
    const res = await request.post(route).send({});
    expect(res.status).toBe(400);
  });

  test("should reject already known email", async () => {
    User.findOne.mockResolvedValue(user);
    await request.post(route).send(user).expect(400);
    expect(User.findOne).toHaveBeenCalled();
  });

  test("should hash the password before saving it", async () => {
    User.findOne.mockResolvedValue(undefined);
    bcrypt.hash.mockResolvedValue(undefined);
    bcrypt.hash.mockResolvedValue("hashedpassword");
    user.save = jest.fn().mockResolvedValue(true);

    let new_user = { ...user, _id: "userid" };
    User.mockReturnValue(new_user);

    await request.post(route).send(user).expect(200);

    expect(bcrypt.genSalt).toHaveBeenCalled();
    expect(bcrypt.hash).toHaveBeenCalled();
    expect(new_user.password).toBe("hashedpassword");
  });

  test("should fail gracefully if database fails when storing password", async () => {
    User.findOne.mockResolvedValue(undefined);
    User.mockReturnValue(user);
    user.save = jest.fn().mockImplementation(() => {
      throw new Error("mock user.save() fail");
    });
    const res = await request.post(route).send(user).expect(500);
    expect(res.body.error).toBe("Server error");
  });

  test("should fail gracefully if database fails when searching user", async () => {
    User.findOne.mockImplementation(() => {
      throw new Error("Database fail");
    });
    const res = await request.post(route).send(user).expect(500);
    expect(res.body.error).toBe("Server error");
  });

  test("should fail gracefully if JWT signing fails", async () => {
    jwt.sign.mockImplementation(() => {
      throw new Error("Fake JWT failure");
    });
    const res = await request.post(route).send(user).expect(500);
    expect(res.body.error).toBe("Server error");
  });
});

describe("profile route", () => {
  test("should reject request without token", async () => {
    const res = await request.get(`${route}/me`).expect(401);
    expect(res.error).not.toBeUndefined();
  });

  test("should reject invalid token", async () => {
    jwt.verify.mockImplementation(() => {
      throw new Error("Invalid token");
    });
    const res = await request
      .get(`${route}/me`)
      .set("x-auth-token", "invalidtoken")
      .expect(401);
    expect(res.error).not.toBeUndefined();
  });

  test("should search user in the database", async () => {
    jwt.verify.mockReturnValue("userId");

    User.findOne = jest.fn().mockImplementation(() => {
      let fakeUser = { firstname: "fakeuser" };
      fakeUser.select = jest.fn().mockImplementation(() => fakeUser);
      return fakeUser;
    });

    const res = await request
      .get(`${route}/me`)
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(res.body.firstname).toBe("fakeuser");
  });

  test("should reject unknown user", async () => {
    jwt.verify.mockReturnValue("userId");
    User.findOne = jest.fn().mockResolvedValue(null);

    const res = await request
      .get(`${route}/me`)
      .set("x-auth-token", "validtoken")
      .expect(400);
    expect(res.body.msg).toBe("Unknown user");
  });

  test("should fail gracefully if database fails while searching user", async () => {
    jwt.verify.mockReturnValue("userId");
    User.findOne.mockImplementation(() => {
      throw new Error("Fake Mongoose failure");
    });
    const res = await request
      .get(`${route}/me`)
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(res.body.error).toBe("Server error");
  });
});
