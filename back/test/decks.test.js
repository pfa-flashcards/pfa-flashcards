const express = require("express");
const app = express();
app.use(express.json());

const { openApiValidator } = require("../openapi");
app.use(openApiValidator);

const route = "/api/decks";
app.use(route, require("../routes/decks.js"));

let request = require("supertest");
request = request(app);

const jwt = require("jsonwebtoken");
jest.mock("jsonwebtoken");

const Card = require("../models/Card");
jest.mock("../models/Card");

const Deck = require("../models/Deck");
jest.mock("../models/Deck");

const User = require("../models/User");
jest.mock("../models/User");

const { isDeckOwner, isCardOwned } = require("../controllers/decks");
const Eval = require("../models/Eval");

const Report = require("../models/Report");
jest.mock("../models/Eval");

describe("deck route", () => {
  let deck;
  beforeEach(() => {
    deck = {
      name: "Anglais",
    };
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test("should be able to get a deck", async () => {
    let deckId = "deckId";
    jwt.verify.mockReturnValue("userId");
    Deck.findById = jest.fn().mockResolvedValue(deck);
    User.populate = jest.fn().mockResolvedValue(deck);
    const res = await request
      .get(`${route}/${deckId}`)
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(res.body.name).toBe("Anglais");
  });

  test("should fail with a unknown deckId", async () => {
    let deckId = "wrongDeckId";
    jwt.verify.mockReturnValue("userId");
    Deck.findById.mockResolvedValue(null);
    const res = await request
      .get(`${route}/${deckId}`)
      .set("x-auth-token", "validtoken")
      .expect(400);
    expect(res.error).toBeDefined();
  });

  test("should fail if database fails while getting a deck", async () => {
    let deckId = "deckId";
    jwt.verify.mockReturnValue("userId");
    Deck.findById.mockImplementation(() =>
      Promise.reject(new Error("Database failed"))
    );
    const res = await request
      .get(`${route}/${deckId}`)
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Deck.findById).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should be able to delete a deck", async () => {
    let deckId = "deckId";
    let deck1 = { cards: [{ _id: "a" }, { _id: "b" }] };
    jwt.verify.mockReturnValue("userId");
    Deck.findById.mockReturnValue(deck1);
    Eval.deleteMany.mockResolvedValue(true);
    Card.findByIdAndDelete.mockResolvedValue(true);
    Report.deleteMany = jest.fn().mockResolvedValue(true);
    Deck.findByIdAndDelete.mockResolvedValue(true);
    Deck.findOne.mockResolvedValue("deck");
    await request
      .delete(`${route}/${deckId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(Deck.findById).toHaveBeenCalled();
    expect(Eval.deleteMany).toHaveBeenCalled();
    expect(Card.findByIdAndDelete).toHaveBeenCalled();
    expect(Report.deleteMany).toHaveBeenCalled();
    expect(Deck.findByIdAndDelete).toHaveBeenCalled();
  });

  test("should fail if the user is not the owner", async () => {
    let deckId = "deckId";
    jwt.verify.mockReturnValue("userId");
    Deck.findOne.mockResolvedValue(null);
    const res = await request
      .delete(`${route}/${deckId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(400);
    expect(Deck.findOne).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should fail if there is a server error", async () => {
    let deckId = "deckId";
    jwt.verify.mockReturnValue("userId");
    Deck.findOne.mockImplementation(() =>
      Promise.reject(new Error("Database failed"))
    );
    const res = await request
      .delete(`${route}/${deckId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Deck.findOne).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should be able to create a deck", async () => {
    let deckId = "deckId";
    let deckName = "deckName";
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    deck.save = jest.fn().mockResolvedValue(true);
    Deck.mockReturnValue({ ...deck, deckId });
    await request
      .post(route)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({ deckName, userId })
      .expect(200);
    expect(deck.save).toHaveBeenCalled();
  });

  test("should fail if database fails while creating a deck", async () => {
    let deckId = "deckId";
    let deckName = "deckName";
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    deck.save = jest
      .fn()
      .mockImplementation(() => Promise.reject(new Error("Database failed")));
    Deck.mockReturnValue({ ...deck, deckId });
    const res = await request
      .post(route)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({ deckName, deckId, userId })
      .expect(500);
    expect(deck.save).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should be able to get all deck cards", async () => {
    let deckId = "deckId";
    let card1 = {
      card: {
        recto: "recto",
        verso: "verso",
      },
    };
    jwt.verify.mockReturnValue("userId");
    Deck.findById = jest.fn().mockResolvedValue(deck);
    Card.find.mockResolvedValue(card1);
    await request
      .get(`${route}/${deckId}/cards`)
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(Deck.findById).toHaveBeenCalled();
    expect(Card.find).toHaveBeenCalled();
  });

  test("should fail with an unknown wrong deckId while getting all deck cards", async () => {
    let deckId = "wrongdeckId";
    jwt.verify.mockReturnValue("userId");
    Deck.findById = jest.fn().mockResolvedValue(null);
    await request
      .get(`${route}/${deckId}/cards`)
      .set("x-auth-token", "validtoken")
      .expect(400);
    expect(Deck.findById).toHaveBeenCalled();
  });

  test("should fail if database fails while getting all deck cards", async () => {
    let deckId = "deckId";
    jwt.verify.mockReturnValue("userId");
    Deck.findById.mockImplementation(() =>
      Promise.reject(new Error("Database failed"))
    );
    Card.find.mockImplementation(() =>
      Promise.reject(new Error("Database failed"))
    );
    const res = await request
      .get(`${route}/${deckId}/cards`)
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Deck.findById).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should be able to get all deck cards sorted", async () => {
    let deckId = "deckId";
    let deck1 = { cards: [{ _id: "a" }, { _id: "b" }] };
    let card1 = {
      card: {
        recto: "recto",
        verso: "verso",
      },
    };
    let eval1 = {
      next_rep_interval: 42,
    };
    jwt.verify.mockReturnValue("userId");
    Deck.findById = jest.fn().mockResolvedValue(deck1);
    Card.findOne.mockResolvedValue(card1);
    Eval.findOne.mockResolvedValue(eval1);
    await request
      .get(`${route}/${deckId}/cards/sorted`)
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(Deck.findById).toHaveBeenCalled();
    expect(Card.findOne).toHaveBeenCalled();
    expect(Eval.findOne).toHaveBeenCalled();
  });

  test("should be able to get all deck cards sorted even if there is no previous eval", async () => {
    let deckId = "deckId";
    let deck1 = { cards: [{ _id: "a" }, { _id: "b" }] };
    let card1 = {
      card: {
        recto: "recto",
        verso: "verso",
      },
    };
    let eval1 = null;
    jwt.verify.mockReturnValue("userId");
    Deck.findById = jest.fn().mockResolvedValue(deck1);
    Card.findOne.mockResolvedValue(card1);
    Eval.findOne.mockResolvedValue(eval1);
    await request
      .get(`${route}/${deckId}/cards/sorted`)
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(Deck.findById).toHaveBeenCalled();
    expect(Card.findOne).toHaveBeenCalled();
    expect(Eval.findOne).toHaveBeenCalled();
  });

  test("should fail with an unknown wrong deckId while getting all deck cards sorted", async () => {
    let deckId = "wrongdeckId";
    jwt.verify.mockReturnValue("userId");
    Deck.findById = jest.fn().mockResolvedValue(null);
    await request
      .get(`${route}/${deckId}/cards/sorted`)
      .set("x-auth-token", "validtoken")
      .expect(400);
    expect(Deck.findById).toHaveBeenCalled();
  });

  test("should fail if database fails while getting all deck cards sorted", async () => {
    let deckId = "deckId";
    jwt.verify.mockReturnValue("userId");
    Deck.findById.mockImplementation(() =>
      Promise.reject(new Error("Database failed"))
    );
    Card.findOne.mockImplementation(() =>
      Promise.reject(new Error("Database failed"))
    );
    const res = await request
      .get(`${route}/${deckId}/cards/sorted`)
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Deck.findById).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should be able to get all user decks", async () => {
    jwt.verify.mockReturnValue("userId");
    Deck.find = jest.fn().mockResolvedValue(deck);
    const res = await request
      .get(`${route}/`)
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(Deck.find).toHaveBeenCalled();
    expect(res.body.name).toBe("Anglais");
  });

  test("should fail if database fails while getting all user decks", async () => {
    jwt.verify.mockReturnValue("userId");
    Deck.find.mockImplementation(() =>
      Promise.reject(new Error("Database failed"))
    );
    const res = await request
      .get(`${route}/`)
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Deck.find).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should find deck with a corresponding pattern", async () => {
    let result = {
      firstname: "fName",
      lastname: "lName",
      email: "Mail@test.fr",
    };
    jwt.verify.mockReturnValue("userId");
    Deck.aggregate.mockResolvedValue([result, result]);
    User.populate.mockResolvedValue(result);
    await request
      .get(`${route}/search/deckId`)
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(Deck.aggregate).toHaveBeenCalled();
    expect(User.populate).toHaveBeenCalled();
  });

  test("should fail if an error occur during search", async () => {
    let result = {
      firstname: "fName",
      lastname: "lName",
      email: "Mail@test.fr",
    };
    jwt.verify.mockReturnValue("userId");
    Deck.aggregate.mockResolvedValue([result]);
    User.populate.mockImplementation(() => {
      return Promise.reject(new Error("Database failed"));
    });
    const res = await request
      .get(`${route}/search/deckId`)
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Deck.aggregate).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should fail if an error occur during getting user profile", async () => {
    jwt.verify.mockReturnValue("userId");
    Deck.aggregate.mockImplementation(() => {
      return Promise.reject(new Error("Database failed"));
    });
    User.populate.mockResolvedValue([]);
    const res = await request
      .get(`${route}/search/deckId`)
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Deck.aggregate).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should be able to autorize a user on a deck", async () => {
    let deckId = "deckId";
    jwt.verify.mockReturnValue("userId");
    Deck.findByIdAndUpdate = jest.fn().mockResolvedValue(deck);
    await request
      .put(`${route}/${deckId}/link`)
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(Deck.findByIdAndUpdate).toHaveBeenCalled();
  });

  test("should fail if database fails while autorizing a user on a deck", async () => {
    let deckId = "deckId";
    jwt.verify.mockReturnValue("userId");
    Deck.findByIdAndUpdate.mockImplementation(() =>
      Promise.reject(new Error("Database failed"))
    );
    const res = await request
      .put(`${route}/${deckId}/link`)
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(res.error).toBeDefined();
  });

  test("should update the privacy", async () => {
    jwt.verify.mockReturnValue("userId");
    Deck.findOne.mockResolvedValue("deck");
    Deck.findByIdAndUpdate.mockResolvedValue(true);
    await request
      .put(`${route}/deckId/privacy/public`)
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(Deck.findByIdAndUpdate).toHaveBeenCalled();
    expect(Deck.findOne).toHaveBeenCalled();
  });

  test("should not update the privacy if not the owner", async () => {
    jwt.verify.mockReturnValue("userId");
    Deck.findOne.mockResolvedValue(null);
    Deck.findByIdAndUpdate.mockResolvedValue(true);
    const res = await request
      .put(`${route}/deckId/privacy/public`)
      .set("x-auth-token", "validtoken")
      .expect(400);
    expect(Deck.findByIdAndUpdate).not.toHaveBeenCalled();
    expect(Deck.findOne).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should fail if there is an error", async () => {
    jwt.verify.mockReturnValue("userId");
    Deck.findOne.mockResolvedValue("deck");
    Deck.findByIdAndUpdate.mockImplementation(() => {
      return Promise.reject(new Error("Database failed"));
    });
    const res = await request
      .put(`${route}/deckId/privacy/public`)
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Deck.findByIdAndUpdate).toHaveBeenCalled();
    expect(res.error).toBeDefined();
    expect(Deck.findOne).toHaveBeenCalled();
  });

  test("should return 200 is the user is the deck owner", async () => {
    jwt.verify.mockReturnValue("userId");
    Deck.findOne.mockResolvedValue("deck");
    const res = await isDeckOwner("userId", "deckId");
    expect(res).toEqual(200);
  });

  test("should return 500 is not the user is the deck owner", async () => {
    jwt.verify.mockReturnValue("userId");
    Deck.findOne.mockResolvedValue(null);
    const res = await isDeckOwner("userId", "deckId");
    expect(res).toEqual(500);
  });

  test("should return 200 is the user is the card owner", async () => {
    jwt.verify.mockReturnValue("userId");
    Deck.findOne.mockResolvedValue("deck");
    const res = await isCardOwned("userId", "cardId");
    expect(res).toEqual(200);
  });

  test("should return 500 is not the user is the card owner", async () => {
    jwt.verify.mockReturnValue("userId");
    Deck.findOne.mockResolvedValue(null);
    const res = await isCardOwned("userId", "cardId");
    expect(res).toEqual(500);
  });
});
