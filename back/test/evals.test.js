const express = require("express");
const app = express();
app.use(express.json());

const { openApiValidator } = require("../openapi");
app.use(openApiValidator);

const route = "/api/evals";
app.use(route, require("../routes/evals.js"));

let request = require("supertest");
request = request(app);

const jwt = require("jsonwebtoken");
jest.mock("jsonwebtoken");

const Eval = require("../models/Eval");
jest.mock("../models/Eval");

const { updateReportAct } = require("../controllers/reports");
jest.mock("../controllers/reports");

describe("evaluation route", () => {
  let evaluation, evaluation1, evaluation2;
  beforeEach(() => {
    evaluation = {
      repetition: 1,
      easiness_factor: 1,
      next_rep_interval: 2,
      scores: [
        {
          easiness: 6,
          date: "Mar 04 2021 10:00:00 AM",
        },
      ],
    };
    evaluation1 = {
      repetition: 0,
      easiness_factor: 1,
      next_rep_interval: 2,
      scores: [
        {
          easiness: 6,
          date: "Mar 04 2021 10:00:00 AM",
        },
      ],
    };
    evaluation2 = {
      repetition: 4,
      easiness_factor: 2,
      next_rep_interval: 2,
      scores: [
        {
          easiness: 6,
          date: "Mar 04 2021 10:00:00 AM",
        },
      ],
    };
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test("should be able to create an eval", async () => {
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    Eval.findOne.mockReturnValue(null);
    evaluation.save = jest.fn().mockResolvedValue(evaluation);
    Eval.mockReturnValue(evaluation);
    evaluation.scores.push = jest.fn().mockResolvedValue(evaluation);
    evaluation.scores.sort = jest.fn().mockResolvedValue(evaluation);
    evaluation.scores.splice = jest.fn().mockResolvedValue(evaluation);
    Eval.findByIdAndUpdate.mockImplementation(
      (filter, update, options, callback) => {
        callback(null);
      }
    );
    updateReportAct.mockResolvedValue(200);
    await request
      .post(route)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({
        cardId: "60253710551f9a10e5e7e388",
        score: 0,
        reportId: "6053828e419b1315b445033b",
        reportScore: 123,
      })
      .expect(200);
    expect(Eval.findOne).toHaveBeenCalled();
    expect(updateReportAct).toHaveBeenCalled();
  });

  test("should be able to update the interval when repetition = 0", async () => {
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    Eval.findOne.mockReturnValue(evaluation1);
    evaluation.save = jest.fn().mockResolvedValue(evaluation);
    evaluation.scores.push = jest.fn().mockResolvedValue(evaluation1);
    evaluation.scores.sort = jest.fn().mockResolvedValue(evaluation1);
    evaluation.scores.splice = jest.fn().mockResolvedValue(evaluation1);
    updateReportAct.mockResolvedValue(200);
    await request
      .post(route)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({
        cardId: "60253710551f9a10e5e7e388",
        score: 3,
        reportId: "6053828e419b1315b445033b",
        reportScore: 123,
      })
      .expect(200);
    expect(Eval.findOne).toHaveBeenCalled();
    expect(updateReportAct).toHaveBeenCalled();
  });

  test("should be able to update the interval when repetition > 1", async () => {
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    Eval.findOne.mockReturnValue(evaluation2);
    evaluation.save = jest.fn().mockResolvedValue(evaluation);
    evaluation.scores.push = jest.fn().mockResolvedValue(evaluation2);
    evaluation.scores.sort = jest.fn().mockResolvedValue(evaluation2);
    evaluation.scores.splice = jest.fn().mockResolvedValue(evaluation2);
    updateReportAct.mockResolvedValue(200);
    await request
      .post(route)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({
        cardId: "60253710551f9a10e5e7e388",
        score: 3,
        reportId: "6053828e419b1315b445033b",
        reportScore: 123,
      })
      .expect(200);
    expect(Eval.findOne).toHaveBeenCalled();
    expect(updateReportAct).toHaveBeenCalled();
  });

  test("should fail if an error occur during saving", async () => {
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    Eval.findOne.mockImplementation(() => {
      throw new Error("Saving in database failed");
    });
    evaluation.save = jest.fn().mockResolvedValue(evaluation);
    evaluation.scores.push = jest.fn().mockResolvedValue(evaluation);
    evaluation.scores.sort = jest.fn().mockResolvedValue(evaluation);
    evaluation.scores.splice = jest.fn().mockResolvedValue(evaluation);
    updateReportAct.mockResolvedValue(200);
    await request
      .post(route)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({
        cardId: "60253710551f9a10e5e7e388",
        score: 2,
        reportId: "6053828e419b1315b445033b",
        reportScore: 123,
      })
      .expect(500);
    expect(Eval.findOne).toHaveBeenCalled();
    expect(updateReportAct).not.toHaveBeenCalled();
  });

  test("should fail if there if wrong information were given", async () => {
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    Eval.findOne.mockReturnValue(evaluation);
    evaluation.save = jest.fn().mockResolvedValue(evaluation);
    evaluation.scores.push = jest.fn().mockResolvedValue(evaluation);
    evaluation.scores.sort = jest.fn().mockResolvedValue(evaluation);
    evaluation.scores.splice = jest.fn().mockResolvedValue(evaluation);
    updateReportAct.mockImplementation(() => {
      return 400;
    });
    await request
      .post(route)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({
        cardId: "60253710551f9a10e5e7e388",
        score: 2,
        reportId: "6053828e419b1315b445033b",
        reportScore: 123,
      })
      .expect(400);
    expect(Eval.findOne).toHaveBeenCalled();
    expect(updateReportAct).toHaveBeenCalled();
  });

  test("should fail if there is an error occur", async () => {
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    Eval.findOne.mockReturnValue(evaluation);
    evaluation.save = jest.fn().mockResolvedValue(evaluation);
    evaluation.scores.push = jest.fn().mockResolvedValue(evaluation);
    evaluation.scores.sort = jest.fn().mockResolvedValue(evaluation);
    evaluation.scores.splice = jest.fn().mockResolvedValue(evaluation);
    updateReportAct.mockImplementation(() => {
      return 500;
    });
    await request
      .post(route)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({
        cardId: "60253710551f9a10e5e7e388",
        score: 2,
        reportId: "6053828e419b1315b445033b",
        reportScore: 123,
      })
      .expect(500);
    expect(Eval.findOne).toHaveBeenCalled();
    expect(updateReportAct).toHaveBeenCalled();
  });

  test("should fail if there is an error occur during saving", async () => {
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    Eval.findOne.mockReturnValue(evaluation);
    evaluation.save = jest.fn().mockResolvedValue(evaluation);
    evaluation.scores.push = jest.fn().mockResolvedValue(evaluation);
    evaluation.scores.sort = jest.fn().mockResolvedValue(evaluation);
    evaluation.scores.splice = jest.fn().mockResolvedValue(evaluation);
    updateReportAct.mockResolvedValue(500);
    await request
      .post(route)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({
        cardId: "60253710551f9a10e5e7e388",
        score: 2,
        reportId: "6053828e419b1315b445033b",
        reportScore: 123,
      })
      .expect(500);
    expect(Eval.findOne).toHaveBeenCalled();
    expect(updateReportAct).toHaveBeenCalled();
  });

  test("should delete eval", async () => {
    let cardId = "cardId";
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    Eval.deleteMany.mockResolvedValue({ deletedCount: 1 });
    await request
      .delete(`${route}/${cardId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(Eval.deleteMany).toHaveBeenCalled();
  });

  test("should fails if an error occur", async () => {
    let cardId = "cardId";
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    Eval.deleteMany.mockImplementation(() => {
      throw new Error("Saving in database failed");
    });
    await request
      .delete(`${route}/${cardId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Eval.deleteMany).toHaveBeenCalled();
  });

  test("should get eval", async () => {
    let cardId = "cardId";
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    Eval.findOne = jest.fn().mockImplementation((filter, callback) => {
      callback(null, evaluation);
    });
    await request
      .get(`${route}/${cardId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(Eval.findOne).toHaveBeenCalled();
  });

  test("should fail if database fails during getting eval", async () => {
    let cardId = "evalId";
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    Eval.findOne = jest.fn().mockImplementation(() => {
      throw new Error("Saving in database failed");
    });
    const res = await request
      .get(`${route}/${cardId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Eval.findOne).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should get last eval", async () => {
    let cardId = "cardId";
    let userId = "userId";
    let score = [
      { id: "1", easiness: 5 },
      { id: "2", easiness: 4 },
      { id: "3", easiness: 7 },
      { id: "4", easiness: 2 },
    ];
    let result = {
      scores: score,
    };
    jwt.verify.mockReturnValue(userId);
    Eval.findOne = jest.fn().mockImplementation((filter, callback) => {
      callback(null, result);
    });
    result.scores.pop = jest.fn().mockResolvedValue({ id: "1", easiness: 5 });
    await request
      .get(`${route}/${cardId}/last`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(Eval.findOne).toHaveBeenCalled();
    expect(result.scores.pop).toHaveBeenCalled();
  });

  test("should fail if database fails during getting last eval", async () => {
    let cardId = "evalId";
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    Eval.findOne = jest.fn().mockImplementation(() => {
      throw new Error("Saving in database failed");
    });
    const res = await request
      .get(`${route}/${cardId}/last`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Eval.findOne).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should fail if database fails during finding eval", async () => {
    let cardId = "evalId";
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    Eval.findOne = jest.fn().mockImplementation((filter, callback) => {
      callback(1, evaluation);
    });
    const res = await request
      .get(`${route}/${cardId}/last`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Eval.findOne).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });
});
