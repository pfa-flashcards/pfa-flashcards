const express = require("express");
const app = express();
app.use(express.json());

const { openApiValidator } = require("../openapi");
app.use(openApiValidator);

const route = "/api/cards";
app.use(route, require("../routes/cards.js"));

let request = require("supertest");
request = request(app);

const jwt = require("jsonwebtoken");
jest.mock("jsonwebtoken");

const Card = require("../models/Card");
jest.mock("../models/Card");

const Deck = require("../models/Deck");
jest.mock("../models/Deck");

const { deleteEvalByCardIdAct } = require("../controllers/evals");
jest.mock("../controllers/evals");

const { isDeckOwner, isCardOwned } = require("../controllers/decks");
jest.mock("../controllers/decks");

describe("card route", () => {
  let card;
  beforeEach(() => {
    card = {
      recto: "recto",
      verso: "verso",
    };
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test("should be able to get a card", async () => {
    let cardId = "cardId";
    jwt.verify.mockReturnValue("userId");
    Card.findById = jest.fn().mockResolvedValue(card);
    const res = await request
      .get(`${route}/${cardId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(res.body.recto).toBe("recto");
  });

  test("should fail with a unknown cardId", async () => {
    let cardId = "wrongCardId";
    jwt.verify.mockReturnValue("userId");
    Card.findById.mockResolvedValue(null);
    const res = await request
      .get(`${route}/${cardId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(400);
    expect(res.error).toBeDefined();
  });

  test("should fail if database fails while getting a card", async () => {
    let cardId = "cardId";
    jwt.verify.mockReturnValue("userId");
    Card.findById.mockImplementation(() =>
      Promise.reject(new Error("Database failed"))
    );
    const res = await request
      .get(`${route}/${cardId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(res.error).toBeDefined();
  });

  test("should be able to create a card", async () => {
    let cardId = "cardId";
    jwt.verify.mockReturnValue("userId");
    Card.insertMany.mockImplementation((card, callback) =>
      callback(null, [card])
    );
    Deck.findByIdAndUpdate.mockResolvedValue({});
    isDeckOwner.mockResolvedValue(200);
    Card.mockReturnValue({ ...card, cardId });
    const deckId = "deckId";
    await request
      .post(route)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({ deckId, card })
      .expect(200);
    expect(Card.insertMany).toHaveBeenCalled();
    expect(isDeckOwner).toHaveBeenCalled();
  });

  test("should fail if the user is not the deck owner", async () => {
    let cardId = "cardId";
    jwt.verify.mockReturnValue("userId");
    Card.insertMany.mockImplementation((card, callback) =>
      callback(null, [card])
    );
    Deck.findByIdAndUpdate.mockResolvedValue({});
    isDeckOwner.mockResolvedValue(500);
    Card.mockReturnValue({ ...card, cardId });
    const deckId = "deckId";
    const res = await request
      .post(route)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({ deckId, card })
      .expect(400);
    expect(Card.insertMany).not.toHaveBeenCalled();
    expect(isDeckOwner).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should reject missing deck id while creating a card", async () => {
    jwt.verify.mockReturnValue("userId");
    const res = await request
      .post(route)
      .set("Content-Type", "application/json")
      .send({})
      .expect(401);
    expect(res.error).toBeDefined();
  });

  test("should fail if database fails to insertMany while creating a card", async () => {
    jwt.verify.mockReturnValue("userId");
    Card.insertMany.mockImplementation((card, callback) => {
      callback(new Error("Database failed"));
    });
    isDeckOwner.mockResolvedValue(200);
    const deckId = "deckId";
    const res = await request
      .post(route)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({ deckId, card })
      .expect(500);
    expect(res.error).toBeDefined();
    expect(isDeckOwner).toHaveBeenCalled();
  });

  test("should fail if database fails to findByIdAndUpdate while creating a card", async () => {
    jwt.verify.mockReturnValue("userId");
    Card.insertMany.mockImplementation((card, callback) =>
      callback(null, [card])
    );
    Deck.findByIdAndUpdate = jest.fn().mockImplementation(() => {
      return Promise.reject(new Error("Database failed"));
    });
    isDeckOwner.mockResolvedValue(200);
    const deckId = "deckId";
    const res = await request
      .post(route)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({ deckId, card })
      .expect(500);
    expect(res.error).toBeDefined();
    expect(isDeckOwner).toHaveBeenCalled();
  });

  test("should fail with an unknown cardId while deleting a card", async () => {
    let cardId = "wrongcardId";
    jwt.verify.mockReturnValue("userId");
    Card.findOneAndDelete = jest.fn().mockImplementation((filter, callback) => {
      callback("error");
    });
    isCardOwned.mockResolvedValue(200);
    const res = await request
      .delete(`${route}/${cardId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(400);
    expect(res.error).toBeDefined();
    expect(isCardOwned).toHaveBeenCalled();
  });

  test("should fail if database fails while deleting a card", async () => {
    let cardId = "wrongcardId";
    jwt.verify.mockReturnValue("userId");
    Card.findOneAndDelete.mockImplementation(() => {
      throw new Error("Database failed");
    });
    isCardOwned.mockResolvedValue(200);
    const res = await request
      .delete(`${route}/${cardId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(res.error).toBeDefined();
    expect(isCardOwned).toHaveBeenCalled();
  });

  test("should delete a card and corresponding evals", async () => {
    let cardId = "cardId";
    jwt.verify.mockReturnValue("userId");
    Card.findOneAndDelete = jest.fn().mockImplementation((filter, callback) => {
      callback(null);
    });
    Deck.updateMany = jest.fn().mockImplementation(() => {
      true;
    });
    deleteEvalByCardIdAct.mockResolvedValue(null);
    isCardOwned.mockResolvedValue(200);
    await request
      .delete(`${route}/${cardId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(Card.findOneAndDelete).toHaveBeenCalled();
    expect(Deck.updateMany).toHaveBeenCalled();
    expect(deleteEvalByCardIdAct).toHaveBeenCalled();
    expect(isCardOwned).toHaveBeenCalled();
  });

  test("should fail to delete if the user is not the owner", async () => {
    let cardId = "cardId";
    jwt.verify.mockReturnValue("userId");
    Card.findOneAndDelete = jest.fn().mockImplementation((filter, callback) => {
      callback(null);
    });
    Deck.updateMany = jest.fn().mockImplementation(() => {
      true;
    });
    deleteEvalByCardIdAct.mockResolvedValue(null);
    isCardOwned.mockResolvedValue(500);
    const res = await request
      .delete(`${route}/${cardId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(400);
    expect(Card.findOneAndDelete).not.toHaveBeenCalled();
    expect(Deck.updateMany).not.toHaveBeenCalled();
    expect(deleteEvalByCardIdAct).not.toHaveBeenCalled();
    expect(isCardOwned).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should delete a card and detect an error during evals deletion", async () => {
    let cardId = "cardId";
    jwt.verify.mockReturnValue("userId");
    Card.findOneAndDelete = jest.fn().mockImplementation((filter, callback) => {
      callback(null);
    });
    Deck.updateMany = jest.fn().mockImplementation(() => {
      true;
    });
    deleteEvalByCardIdAct.mockImplementation(() => {
      return 500;
    });
    isCardOwned.mockResolvedValue(200);
    await request
      .delete(`${route}/${cardId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Card.findOneAndDelete).toHaveBeenCalled();
    expect(deleteEvalByCardIdAct).toHaveBeenCalled();
    expect(isCardOwned).toHaveBeenCalled();
  });

  test("should delete a card and detect an unexpected event occur during evals deletion", async () => {
    let cardId = "cardId";
    jwt.verify.mockReturnValue("userId");
    Card.findOneAndDelete = jest.fn().mockImplementation((filter, callback) => {
      callback(null);
    });
    Deck.updateMany = jest.fn().mockImplementation(() => {
      true;
    });
    deleteEvalByCardIdAct.mockImplementation(() => {
      return 500;
    });
    isCardOwned.mockResolvedValue(200);
    await request
      .delete(`${route}/${cardId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Card.findOneAndDelete).toHaveBeenCalled();
    expect(deleteEvalByCardIdAct).toHaveBeenCalled();
    expect(isCardOwned).toHaveBeenCalled();
  });

  test("should update a card", async () => {
    let cardId = "cardId";
    jwt.verify.mockReturnValue("userId");
    let newcard = {
      card: {
        recto: "newrecto",
        verso: "newverso",
      },
    };
    Card.findByIdAndUpdate.mockResolvedValue(newcard);
    isCardOwned.mockResolvedValue(200);
    await request
      .put(`${route}/${cardId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send(newcard)
      .expect(200);
    expect(Card.findByIdAndUpdate).toHaveBeenCalled();
    expect(isCardOwned).toHaveBeenCalled();
  });

  test("should fail if the updated card is not owned by user", async () => {
    let cardId = "cardId";
    jwt.verify.mockReturnValue("userId");
    let newcard = {
      card: {
        recto: "newrecto",
        verso: "newverso",
      },
    };
    Card.findByIdAndUpdate.mockResolvedValue(newcard);
    isCardOwned.mockResolvedValue(500);
    const res = await request
      .put(`${route}/${cardId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send(newcard)
      .expect(400);
    expect(Card.findByIdAndUpdate).not.toHaveBeenCalled();
    expect(isCardOwned).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should fail if database fails when updating a card", async () => {
    let cardId = "cardId";
    jwt.verify.mockReturnValue("userId");
    Card.findByIdAndUpdate.mockImplementation(() =>
      Promise.reject(new Error("Database failed"))
    );
    isCardOwned.mockResolvedValue(200);
    const res = await request
      .put(`${route}/${cardId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({ card })
      .expect(500);
    expect(res.error).toBeDefined();
  });
});
