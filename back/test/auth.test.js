const express = require("express");
const app = express();
app.use(express.json());

const { openApiValidator } = require("../openapi");
app.use(openApiValidator);

const route = "/api/auth";
app.use(route, require("../routes/auth.js"));

let request = require("supertest");
request = request(app);

const jwt = require("jsonwebtoken");
jest.mock("jsonwebtoken");

const User = require("../models/User");
jest.mock("../models/User");
const bcrypt = require("bcryptjs");
jest.mock("bcryptjs");

describe("authentication route", () => {
  test("should reject empty request", async () => {
    const res = await request.post(route).send({});
    expect(res.status).toBe(400);
  });

  test("should reject missing password", async () => {
    const res = await request.post(route).send({ email: "abc@gmail.com" });
    expect(res.status).toBe(400);
  });

  test("should reject missing email", async () => {
    const res = await request.post(route).send({ password: "password" });
    expect(res.status).toBe(400);
  });

  test("should reject unknown users", async () => {
    User.findOne.mockResolvedValue(null);

    const res = await request
      .post(route)
      .set("Content-Type", "application/json")
      .send({ email: "abc@mail.fr", password: "password" })
      .expect(400);

    expect(res.body.msg).not.toBeUndefined();
  });

  test("should reject wrong passwords", async () => {
    User.findOne.mockResolvedValue({});
    bcrypt.compare.mockResolvedValue(false);

    const res = await request
      .post(route)
      .set("Content-Type", "application/json")
      .send({ email: "abc@mail.fr", password: "wrongpassword" })
      .expect(400);

    expect(res.body.msg).not.toBeUndefined();
  });

  test("should send back an authentication token on success", async () => {
    jwt.sign.mockReturnValue("userId");
    User.findOne.mockResolvedValue({
      _id: "12345678910",
    });
    bcrypt.compare.mockResolvedValue(true);

    const res = await request
      .post(route)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({
        email: "abc@gmail.com",
        password: "password",
      })
      .expect(200);

    expect(res.body.token).not.toBeUndefined();
  });

  test("should fail gracefully if database fails", async () => {
    jwt.sign.mockReturnValue("userId");
    User.findOne.mockImplementation(() => {
      throw new Error("Fake Mongoose failure");
    });
    const res = await request
      .post(route)
      .set("x-auth-token", "validtoken")
      .send({
        email: "abc@gmail.com",
        password: "password",
      })
      .expect(500);
    expect(res.body.error).toBe("Server error");
  });
});
