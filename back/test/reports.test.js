const express = require("express");
const app = express();
app.use(express.json());

const { openApiValidator } = require("../openapi");
app.use(openApiValidator);

const route = "/api/reports";
app.use(route, require("../routes/reports.js"));

let request = require("supertest");
request = request(app);

const jwt = require("jsonwebtoken");
jest.mock("jsonwebtoken");

const Report = require("../models/Report");
jest.mock("../models/Report");

const Deck = require("../models/Deck");
jest.mock("../models/Deck");

const User = require("../models/User");
jest.mock("../models/User");

describe("report route", () => {
  let rep;
  beforeEach(() => {
    rep = {
      score: 6,
    };
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test("should be able to create a report", async () => {
    let repId = "repId";
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    rep.save = jest.fn().mockResolvedValue(true);
    Report.mockReturnValue({ ...rep, repId });
    await request
      .post(route)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({
        deckId: "60253710551f9a10e5e7e388",
        score: "2",
      })
      .expect(200);
    expect(rep.save).toHaveBeenCalled();
  });

  test("should return an error if an error occur during creation", async () => {
    let repId = "repId";
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    rep.save = jest.fn().mockImplementation(() => {
      throw new Error("Saving in database failed");
    });
    Report.mockReturnValue({ ...rep, repId });
    const res = await request
      .post(route)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({
        deckId: "deckId",
        score: "2",
      })
      .expect(500);
    expect(rep.save).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should be able to get reports", async () => {
    let repId = "repId";
    let userId = "userId";
    let deckId = "deckId";
    jwt.verify.mockReturnValue(userId);
    Report.find = jest.fn().mockResolvedValue(true);
    Report.mockReturnValue({ ...rep, repId });
    await request
      .get(`${route}/deck/${deckId}`)
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(Report.find).toHaveBeenCalled();
  });

  test("should return an error if an error occur during getting", async () => {
    let repId = "repId";
    let userId = "userId";
    let deckId = "deckId";
    jwt.verify.mockReturnValue(userId);
    Report.find = jest.fn().mockImplementation(() => {
      throw new Error("Saving in database failed");
    });
    Report.mockReturnValue({ ...rep, repId });
    const res = await request
      .get(`${route}/deck/${deckId}`)
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Report.find).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should be able to delete reports", async () => {
    let repId = "repId";
    let userId = "userId";
    let deckId = "deckId";
    jwt.verify.mockReturnValue(userId);
    Report.deleteMany = jest.fn().mockResolvedValue(true);
    Report.mockReturnValue({ ...rep, repId });
    await request
      .delete(`${route}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({
        deckId: deckId,
      })
      .expect(200);
    expect(Report.deleteMany).toHaveBeenCalled();
  });

  test("should return an error if an error occur during deletion", async () => {
    let repId = "repId";
    let userId = "userId";
    let deckId = "deckId";
    jwt.verify.mockReturnValue(userId);
    Report.deleteMany = jest.fn().mockImplementation(() => {
      throw new Error("Saving in database failed");
    });
    Report.mockReturnValue({ ...rep, repId });
    const res = await request
      .delete(`${route}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({
        deckId: deckId,
      })
      .expect(500);
    expect(Report.deleteMany).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should update a report", async () => {
    let repId = "repId";
    let userId = "userId";
    let score = "scoreId";
    jwt.verify.mockReturnValue({ _id: userId });
    Report.findOneAndUpdate.mockResolvedValue(null, "result");
    Report.mockReturnValue({ ...rep, repId });
    await request
      .put(`${route}/${repId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({
        score: score,
      })
      .expect(200);
    expect(Report.findOneAndUpdate).toHaveBeenCalled();
  });

  test("should return an error if an error occur while updating", async () => {
    let repId = "repId";
    let userId = "userId";
    let score = "scoreId";
    jwt.verify.mockReturnValue(userId);
    Report.findOneAndUpdate.mockImplementation(() => {
      throw new Error("Saving in database failed");
    });
    Report.mockReturnValue({ ...rep, repId });
    const res = await request
      .put(`${route}/${repId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({
        score: score,
      })
      .expect(500);
    expect(Report.findOneAndUpdate).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should return an error if wrong data were given", async () => {
    let repId = "repId";
    let userId = "userId";
    let score = "scoreId";
    jwt.verify.mockReturnValue(userId);
    Report.findOneAndUpdate.mockResolvedValue(null, "result");
    Report.mockReturnValue({ ...rep, repId });
    const res = await request
      .put(`${route}/${repId}`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .send({
        score: score,
      })
      .expect(200);
    expect(Report.findOneAndUpdate).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should get last evals", async () => {
    let deckId = "deckId";
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    Report.find = jest.fn().mockImplementation(() => {
      return rep;
    });
    rep.sort = jest.fn().mockImplementation(() => {
      return rep;
    });
    rep.limit = jest.fn().mockResolvedValue(true);
    await request
      .get(`${route}/deck/${deckId}/last`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(Report.find).toHaveBeenCalled();
    expect(rep.sort).toHaveBeenCalled();
    expect(rep.limit).toHaveBeenCalled();
  });

  test("should fail if database fails during getting of last eval", async () => {
    let deckId = "deckId";
    let userId = "userId";
    jwt.verify.mockReturnValue(userId);
    Report.find = jest.fn().mockImplementation(() => {
      throw new Error("Saving in database failed");
    });
    const res = await request
      .get(`${route}/deck/${deckId}/last`)
      .set("Content-Type", "application/json")
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Report.find).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should be able to all reports", async () => {
    let repId = "repId";
    let userId = "userId";
    let deckId = "deckId";
    jwt.verify.mockReturnValue(userId);
    Deck.findOne = jest.fn().mockResolvedValue([]);
    Report.find = jest.fn().mockResolvedValue([]);
    User.populate = jest.fn().mockResolvedValue([]);
    Report.mockReturnValue({ ...rep, repId });
    await request
      .get(`${route}/deck/${deckId}/author`)
      .set("x-auth-token", "validtoken")
      .expect(200);
    expect(Report.find).toHaveBeenCalled();
    expect(Deck.findOne).toHaveBeenCalled();
    expect(User.populate).toHaveBeenCalled();
  });

  test("should return nothing if the user is not the author", async () => {
    let repId = "repId";
    let userId = "userId";
    let deckId = "deckId";
    jwt.verify.mockReturnValue(userId);
    Deck.findOne = jest.fn().mockResolvedValue(null);
    Report.find = jest.fn().mockResolvedValue([]);
    User.populate = jest.fn().mockResolvedValue([]);
    Report.mockReturnValue({ ...rep, repId });
    await request
      .get(`${route}/deck/${deckId}/author`)
      .set("x-auth-token", "validtoken")
      .expect(400);
    expect(Report.find).not.toHaveBeenCalled();
    expect(Deck.findOne).toHaveBeenCalled();
    expect(User.populate).not.toHaveBeenCalled();
  });

  test("should end if an error occur during verification", async () => {
    let repId = "repId";
    let userId = "userId";
    let deckId = "deckId";
    jwt.verify.mockReturnValue(userId);
    Deck.findOne = jest.fn().mockImplementation(() => {
      throw new Error("Saving in database failed");
    });
    Report.find = jest.fn().mockResolvedValue([]);
    User.populate = jest.fn().mockResolvedValue([]);
    Report.mockReturnValue({ ...rep, repId });
    const res = await request
      .get(`${route}/deck/${deckId}/author`)
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Report.find).not.toHaveBeenCalled();
    expect(Deck.findOne).toHaveBeenCalled();
    expect(User.populate).not.toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should end if an error occur during report finding", async () => {
    let repId = "repId";
    let userId = "userId";
    let deckId = "deckId";
    jwt.verify.mockReturnValue(userId);
    Deck.findOne = jest.fn().mockResolvedValue([]);
    Report.find = jest.fn().mockImplementation(() => {
      throw new Error("Saving in database failed");
    });
    User.populate = jest.fn().mockResolvedValue([]);
    Report.mockReturnValue({ ...rep, repId });
    const res = await request
      .get(`${route}/deck/${deckId}/author`)
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Report.find).toHaveBeenCalled();
    expect(Deck.findOne).toHaveBeenCalled();
    expect(User.populate).not.toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });

  test("should end if an error occur during populate", async () => {
    let repId = "repId";
    let userId = "userId";
    let deckId = "deckId";
    jwt.verify.mockReturnValue(userId);
    Deck.findOne = jest.fn().mockResolvedValue([]);
    Report.find = jest.fn().mockResolvedValue([]);
    User.populate = jest.fn().mockImplementation(() => {
      return Promise.reject(new Error("Database failed"));
    });

    Report.mockReturnValue({ ...rep, repId });
    const res = await request
      .get(`${route}/deck/${deckId}/author`)
      .set("x-auth-token", "validtoken")
      .expect(500);
    expect(Report.find).toHaveBeenCalled();
    expect(Deck.findOne).toHaveBeenCalled();
    expect(User.populate).toHaveBeenCalled();
    expect(res.error).toBeDefined();
  });
});
