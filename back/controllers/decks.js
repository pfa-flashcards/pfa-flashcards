const Deck = require("../models/Deck");
const Card = require("../models/Card");
const User = require("../models/User");
const Eval = require("../models/Eval");
const Report = require("../models/Report");

function createDeck(req, res) {
  const name = req.body.name;
  const userId = req.userId;
  const docDeck = new Deck({ name });
  docDeck.owner = userId;

  docDeck
    .save()
    .then((result) => {
      return res.status(200).json(result);
    })
    .catch(() => {
      return res.status(500).json({ error: "Server error" });
    });
}

function getUserDecks(req, res) {
  const userId = req.userId;
  Deck.find({ $or: [{ owner: userId }, { authorized: userId }] })
    .then((result) => {
      return res.status(200).json(result);
    })
    .catch(() => {
      return res.status(500).json({ error: "Server error" });
    });
}

function getDeck(req, res) {
  const deckId = req.params.deckId;
  Deck.findById({ _id: deckId })
    .then((deck) => {
      if (!deck) {
        return res.status(400).json({ msg: "unknown DeckId" });
      } else {
        User.populate(deck, {
          path: "owner",
          select: "firstname lastname -_id",
        }).then((result) => {
          return res.status(200).json(result);
        });
      }
    })
    .catch(() => {
      return res.status(500).json({ error: "Server error" });
    });
}

async function deleteDeck(req, res) {
  const deckId = req.params.deckId;
  const userId = req.userId;
  try {
    let owned = await isDeckOwner(userId, deckId);
    if (owned == 200) {
      let deck = await Deck.findById({ _id: deckId });
      for (const cardId of deck.cards) {
        await Eval.deleteMany({ card: cardId });
        await Card.findByIdAndDelete(cardId);
      }
      await Report.deleteMany({ deck: deckId });
      await Deck.findByIdAndDelete(deckId);
      return res.status(200).json("Deck deleted");
    } else {
      return res.status(400).json({ error: "Not the owner" });
    }
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Server error" });
  }
}

function getDeckCards(req, res) {
  const deckId = req.params.deckId;
  Deck.findById({ _id: deckId })
    .then((result) => {
      if (!result) {
        return res.status(400).json({ msg: "unknown DeckId" });
      } else {
        const cardsId = result.cards;
        Card.find({ _id: { $in: cardsId } }).then((result) => {
          return res.status(200).json(result);
        });
      }
    })
    .catch(() => {
      return res.status(500).json({ error: "Server error" });
    });
}

async function getDeckCardsSorted(req, res) {
  const deckId = req.params.deckId;
  const userId = req.userId;
  try {
    let result = await Deck.findById({ _id: deckId });
    if (!result) {
      return res.status(400).json({ msg: "unknown DeckId" });
    } else {
      let cardsId = result.cards;
      cardsId = await Promise.all(
        cardsId.map(async (x) => {
          let e = await Eval.findOne({ card: x._id, user: userId });
          e = e === null ? { next_rep_interval: 0 } : e;
          let c = await Card.findOne({ _id: x });
          return [c, e];
        })
      );
      let cardsIdSorted = cardsId.sort((a, b) => {
        return a[1].next_rep_interval - b[1].next_rep_interval;
      });
      let cardsSorted = cardsIdSorted.map((a) => a[0]);
      console.log(cardsSorted);
      return res.status(200).json(cardsSorted);
    }
  } catch (err) {
    return res.status(500).json({ error: "Server error" });
  }
}

async function searchDeckByPattern(req, res) {
  const pattern = req.params.pattern;

  try {
    let decks = await Deck.aggregate([
      {
        $project: {
          _id: 1,
          nbCards: { $size: "$cards" },
          nbAuthorized: { $size: "$authorized" },
          owner: 1,
          name: 1,
          private: 1,
        },
      },
      { $match: { name: { $regex: pattern, $options: "i" }, private: false } },
    ]);

    decks = await User.populate(decks, {
      path: "owner",
      select: "firstname lastname _id",
    });

    return res.status(200).json(decks);
  } catch (err) {
    return res.status(500).json({ error: "Server error" });
  }
}

function linkUserToDeck(req, res) {
  const deckId = req.params.deckId;
  const userId = req.userId;
  Deck.findByIdAndUpdate(
    deckId,
    { $push: { authorized: userId } },
    { useFindAndModify: false }
  )
    .then((result) => {
      return res.status(200).json(result);
    })
    .catch(() => {
      return res.status(500).json({ error: "Server error" });
    });
}

async function setDeckPrivacy(req, res) {
  const deckId = req.params.deckId;
  const privacy = req.params.privacy;
  const userId = req.userId;

  let ownership = await isDeckOwner(userId, deckId);
  if (ownership == 200) {
    Deck.findByIdAndUpdate(
      deckId,
      { $set: { private: privacy == "private" ? true : false } },
      { useFindAndModify: false }
    )
      .then(() => {
        return res.status(200).json({ msg: "Update was successfull" });
      })
      .catch(() => {
        return res.status(500).json({ error: "Server error" });
      });
  } else {
    return res.status(400).json({ error: "Not the owner" });
  }
}

async function isDeckOwner(userId, deckId) {
  let res = await Deck.findOne({ _id: deckId, owner: userId });
  if (res) {
    return 200;
  } else {
    return 500;
  }
}

async function isCardOwned(userId, cardId) {
  let res = await Deck.findOne({ cards: cardId, owner: userId });
  if (res) {
    return 200;
  } else {
    return 500;
  }
}

module.exports = {
  createDeck,
  getUserDecks,
  getDeck,
  deleteDeck,
  getDeckCards,
  getDeckCardsSorted,
  searchDeckByPattern,
  linkUserToDeck,
  setDeckPrivacy,
  isDeckOwner,
  isCardOwned,
};
