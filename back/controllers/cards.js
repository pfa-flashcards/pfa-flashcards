const Deck = require("../models/Deck");
const Card = require("../models/Card");
const { deleteEvalByCardIdAct } = require("./evals");
const { isDeckOwner, isCardOwned } = require("./decks");

async function createCard(req, res) {
  const deckId = req.body.deckId;
  const card = req.body.card;
  const userId = req.userId;
  let owned = await isDeckOwner(userId, deckId);
  if (owned == 200) {
    Card.insertMany(card, (error, result) => {
      if (error) {
        return res.status(500).json(error);
      }
      Deck.findByIdAndUpdate(
        deckId,
        { $push: { cards: { $each: result } } },
        { new: true, useFindAndModify: false }
      )
        .then((res2) => {
          return res.status(200).json(res2);
        })
        .catch((err) => {
          return res.status(500).json(err);
        });
    });
  } else {
    return res.status(400).json({ error: "Not the owner" });
  }
}

async function updateCard(req, res) {
  const cardId = req.params.card_id;
  const card = req.body.card;
  const userId = req.userId;
  let owned = await isCardOwned(userId, cardId);
  if (owned == 200) {
    Card.findByIdAndUpdate(cardId, card, { useFindAndModify: false })
      .then((result) => {
        return res.status(200).json(result);
      })
      .catch((err) => {
        console.log(err);
        return res.status(500).json({ error: "Server error" });
      });
  } else {
    return res.status(400).json({ error: "Not the owner" });
  }
}

async function deleteCard(req, res) {
  const cardId = req.params.card_id;
  const userId = req.userId;

  try {
    let owned = await isCardOwned(userId, cardId);
    if (owned == 200) {
      await Card.findOneAndDelete({ _id: cardId }, async (err) => {
        if (err) {
          return res.status(400).json({ error: "Unknown CardId" });
        } else {
          await Deck.updateMany(
            { cards: cardId },
            { $pull: { cards: cardId } },
            { useFindAndModify: false }
          );
          let delRes = deleteEvalByCardIdAct(cardId, userId);
          if (delRes == 500) {
            return res.status(500).json({ error: "Server error" });
          } else {
            return res.status(200).json("Card deleted");
          }
        }
      });
    } else {
      return res.status(400).json({ error: "Not the owner" });
    }
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Server error" });
  }
}

// create route for getCard (/api/cards/$card_id)
function getCard(req, res) {
  const cardId = req.params.card_id;
  Card.findById({ _id: cardId })
    .then((result) => {
      if (!result) {
        return res.status(400).json({ msg: "unknown CardId" });
      } else {
        return res.status(200).json(result);
      }
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({ error: "Server error" });
    });
}

module.exports = {
  createCard,
  getCard,
  updateCard,
  deleteCard,
};
