const Report = require("../models/Report");
const Deck = require("../models/Deck");
const User = require("../models/User");

// Create a report containing the result of the revision session
// And return it
function createReport(req, res) {
  const deckId = req.body.deckId;
  const userId = req.userId;
  const score = req.body.score;

  const docReport = new Report({
    user: userId,
    deck: deckId,
    score: score,
  });

  try {
    docReport.save().then((result) => {
      console.log(`${userId} scored ${score} on deck ${deckId} `);
      return res.status(200).json(result);
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Server error" });
  }
}

function updateReportAct(userId, reportId, score, nbCards) {
  try {
    Report.findOneAndUpdate(
      { _id: reportId, user: userId },
      { $set: { score: score }, $inc: { nbCards: nbCards } },
      { useFindAndModify: false }
    ).then(() => {
      console.log(`Report ${reportId} has been update`);
    });
  } catch (err) {
    console.log(err);
    return 500;
  }
}

function updateReport(req, res) {
  const reportId = req.params.reportId;
  const score = req.body.score;
  const userId = req.userId;

  const upRes = updateReportAct(userId, reportId, score, 0);
  if (upRes == 500) {
    return res.status(500).json({ error: "Server error" });
  } else {
    return res.status(200).json(upRes);
  }
}

function getReportsByDeckId(req, res) {
  const deckId = req.params.deckId;
  const userId = req.userId;

  try {
    Report.find({ deck: deckId, user: userId }).then((result) => {
      console.log(`${userId} scored on deck ${deckId} were sended`, result);
      return res.status(200).json(result);
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Server error" });
  }
}

function getLastReport(req, res) {
  const deckId = req.params.deckId;
  const userId = req.userId;

  try {
    Report.find({ deck: deckId })
      .sort({ date: -1 })
      .limit(1)
      .then((result) => {
        console.log(`${userId} last Report on deck ${deckId}`, result);
        return res.status(200).json(result);
      });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Server error" });
  }
}

function deleteReportsByDeckId(req, res) {
  const deckId = req.body.deckId;
  const userId = req.userId;

  try {
    Report.deleteMany({ deck: deckId, user: userId }).then((result) => {
      console.log(`${userId} scored on deck ${deckId} were deleted`, result);
      return res.status(200).json(result);
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Server error" });
  }
}

async function getReportsByDeckIdForAuthor(req, res) {
  const deckId = req.params.deckId;
  const userId = req.userId;

  try {
    const deck = await Deck.findOne({ _id: deckId, owner: userId });
    if (!deck) return res.status(400).json({ msg: "User is not the author" });

    let reports = Report.find({ deck: deckId });
    reports = await User.populate(reports, {
      path: "user",
      select: "firstname lastname -_id",
    });

    return res.status(200).json(reports);
  } catch (err) {
    return res.status(500).json({ error: "Server error" });
  }
}

module.exports = {
  createReport,
  updateReport,
  updateReportAct,
  getReportsByDeckId,
  getLastReport,
  deleteReportsByDeckId,
  getReportsByDeckIdForAuthor,
};
