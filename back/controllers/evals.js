const Eval = require("../models/Eval");
const { updateReportAct } = require("./reports");

// Create a new evalutation for a given card and a given user

async function createEval(req, res) {
  const cardId = req.body.cardId;
  const userId = req.userId;
  const score = req.body.score;
  const reportId = req.body.reportId;
  const reportScore = req.body.reportScore;
  let interval, easiness_factor;

  try {
    let evaluation = await Eval.findOne({ card: cardId, user: userId });

    if (!evaluation) {
      evaluation = new Eval({ card: cardId, user: userId });
    }

    interval = getInterval(evaluation);
    easiness_factor = getEasinessFactor(score, evaluation.easiness_factor);
    evaluation.scores.push({ easiness: score });
    evaluation.scores.sort((a, b) => {
      return b.date - a.date;
    });
    evaluation.scores.splice(10);

    if (score >= 1) {
      evaluation.next_rep_interval = Math.ceil(interval);
      evaluation.easiness_factor = easiness_factor;
      evaluation.repetition++;
    } else {
      evaluation.repetition = 0;
      evaluation.next_rep_interval = 1;
    }
    evaluation = new Eval(evaluation);
    await evaluation.save({ upsert: true });
    console.log("Priority updated");
    const upRes = await updateReportAct(userId, reportId, reportScore, 1);
    if (upRes == 400) {
      return res.status(400).json({ error: "Wrong data were provided" });
    } else if (upRes == 500) {
      return res.status(500).json({ error: "Server error" });
    } else {
      return res.status(200).json("Success");
    }
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Server error" });
  }
}

function getInterval(evaluation) {
  let interval;
  switch (evaluation.repetition) {
    case 0:
      interval = 1;
      break;
    case 1:
      interval = 6;
      break;
    default:
      interval = evaluation.next_rep_interval * evaluation.easiness_factor;
      break;
  }
  return interval;
}

function getEasinessFactor(score, oldEasinessFactor) {
  let easiness_factor =
    oldEasinessFactor + 0.1 - (3 - score) * (0.08 + (3 - score) * 0.02);
  if (easiness_factor < 1.3) easiness_factor = 1.3;
  return easiness_factor;
}

function deleteEvalByCardIdAct(cardId, userId) {
  try {
    Eval.deleteMany({ card: cardId, user: userId }).then((result) => {
      console.log(
        `${result.deletedCount} scores on card ${cardId} were deleted`
      );
    });
  } catch (err) {
    console.log(err);
    return 500;
  }
}

function deleteEvalByCardId(req, res) {
  const cardId = req.params.cardId;
  const userId = req.userId;

  const delRes = deleteEvalByCardIdAct(cardId, userId);
  if (delRes == 500) {
    return res.status(500).json({ error: "Server error" });
  } else {
    return res.status(200).json("Evals deleted");
  }
}

function getEval(req, res) {
  const cardId = req.params.cardId;
  const userId = req.userId;

  try {
    Eval.findOne({ card: cardId, user: userId }, (err, result) => {
      console.log(`${userId} last Eval on card ${cardId}`, result);
      return res.status(200).json(result);
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Server error" });
  }
}

function getLastEval(req, res) {
  const cardId = req.params.cardId;
  const userId = req.userId;

  try {
    Eval.findOne({ card: cardId, user: userId }, (err, result) => {
      if (err) {
        return res.status(500).json({ error: "Server error" });
      } else {
        return res.status(200).json(result.scores.pop());
      }
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Server error" });
  }
}

module.exports = {
  createEval,
  getEval,
  getLastEval,
  deleteEvalByCardId,
  deleteEvalByCardIdAct,
};
