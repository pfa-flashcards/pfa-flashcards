const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");
const User = require("../models/User");

/**
 * Send a JWT for the current user
 * @param {Request} req a request containing a userId
 */
function sendJWT(req, res) {
  const payload = {
    _id: req.userId,
  };

  const token = jwt.sign(payload, config.get("jwtSecret"), {
    expiresIn: "2 days",
  });
  res.status(200).json({ token });
}

/**
 * Register a new user and send a token
 * @param {Request} req a request containing the required sign-up values
 */
async function signUp(req, res) {
  const { firstname, lastname, email, password } = req.body;

  try {
    // Search for already registered user
    let user = await User.findOne({ email });
    if (user) {
      return res.status(400).json({ error: "User already registered" });
    }

    user = new User({ firstname, lastname, email, password });
    // Encrypt password
    const salt = await bcrypt.genSalt();
    user.password = await bcrypt.hash(password, salt);

    // Register user
    await user.save();

    // Return JWT
    req.userId = user._id;
    sendJWT(req, res);
  } catch (err) {
    return res.status(500).json({ error: "Server error" });
  }
}

/**
 * Authenticate a user and send a token
 * @param {Request} req a request containing the required sign-in values
 */
async function signIn(req, res) {
  const { email, password } = req.body;

  try {
    let user = await User.findOne({ email });
    if (!user) {
      return res.status(400).json({ msg: "Invalid credentials" });
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      return res.status(400).json({ msg: "Invalid credentials" });
    }

    req.userId = user._id;
    sendJWT(req, res);
  } catch (err) {
    return res.status(500).json({ error: "Server error" });
  }
}

/**
 *
 * @param {Request} req a request containing a userId
 */
async function getProfile(req, res) {
  try {
    const user = await User.findOne(
      {
        _id: req.userId,
      },
      "firstname lastname email -_id"
    );

    if (!user) {
      return res.status(400).json({ msg: "Unknown user" });
    }

    res.status(200).json(user);
  } catch (err) {
    res.status(500).json({ error: "Server error" });
  }
}

module.exports = { sendJWT, signUp, signIn, getProfile };
