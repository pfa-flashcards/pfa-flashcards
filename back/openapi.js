const swaggerJsdoc = require("swagger-jsdoc");

const options = {
  swaggerDefinition: {
    openapi: "3.0.3",
    info: {
      title: "PFA Flashcards",
      version: "0.0.1",
    },
    security: [
      {
        "JWT Authentication": [],
      },
    ],
    servers: [
      {
        url: "http://localhost:3000/api",
        description: "Development server",
      },
    ],
  },
  apis: ["./routes/*.yml"],
};

const swaggerSpecification = swaggerJsdoc(options);
const OpenApiValidator = require("express-openapi-validator");

const openApiValidator = OpenApiValidator.middleware({
  apiSpec: swaggerSpecification,
  validateRequests: true, // (default)
  validateResponses: false, // false by default
});

module.exports = { swaggerSpecification, openApiValidator };
