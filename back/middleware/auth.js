const jwt = require("jsonwebtoken");
const config = require("config");

/**
 * Extract, verify and decode JWT from header
 */
function authJWT(req, res, next) {
  try {
    const token = req.headers["x-auth-token"];
    req.userId = jwt.verify(token, config.get("jwtSecret"));
    next();
  } catch (err) {
    res.status(401).json({ error: "Token is invalid" });
  }
}

module.exports = authJWT;
