const express = require("express");
const cors = require("cors");
const path = require("path");
const fs = require("fs");

const buildDir = "../front/build";

const { swaggerSpecification, openApiValidator } = require("./openapi");
const swaggerUi = require("swagger-ui-express");

// Initialize ExpressJS
const app = express();
app.use(express.json());
app.use(cors());

// Use OpenAPI validator middleware
app.use(openApiValidator);

// Serve routes
app.use("/api/users", require("./routes/users"));
app.use("/api/auth", require("./routes/auth"));
app.use("/api/reports", require("./routes/reports"));
app.use("/api/evals", require("./routes/evals"));
app.use("/api/cards", require("./routes/cards"));
app.use("/api/decks", require("./routes/decks"));

// Serve OpenAPI documentation
app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerSpecification));

// Serve compiled front appllication if available
if (fs.existsSync(buildDir)) {
  app.use(express.static(path.join(__dirname, buildDir)));

  app.get("/*", function (req, res) {
    res.sendFile(path.join(__dirname, buildDir, "index.html"));
  });
}

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  res.status(err.status || 500).json({
    message: err.message,
    errors: err.errors,
  });
});

module.exports = app;
