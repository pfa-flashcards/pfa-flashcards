const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const evalSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  card: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Card",
    required: true,
  },
  repetition: {
    type: Number,
    default: 0,
    required: true,
  },
  easiness_factor: {
    type: Number,
    default: 1.5,
    required: true,
  },
  next_rep_interval: {
    type: Number,
    default: 1,
    required: true,
  },
  scores: [
    {
      easiness: { type: Number, required: true },
      date: { type: Date, default: Date.now },
    },
  ],
});

const Eval = mongoose.model("Eval", evalSchema);
module.exports = Eval;
