const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const reportSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  deck: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Deck",
    required: true,
  },
  score: {
    type: Number,
    required: true,
  },
  nbCards: {
    type: Number,
    required: true,
    default: 0,
  },
  date: { type: Date, default: Date.now },
});

const Report = mongoose.model("Report", reportSchema);
module.exports = Report;
