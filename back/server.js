const mongoose = require("mongoose");
const app = require("./main");
const config = require("config");

// Server constants
const HOST = config.get("HOST");
const PORT = process.env.PORT || config.get("PORT");

// MongoDB const
const dbPath = config.get("dbPath");
const dbOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  ...config.get("dbOptions"),
};

// Initialize Mongoose connection
mongoose
  .connect(dbPath, dbOptions)
  .then(() => {
    console.log("Successfully connected to MongoDB");
  })
  .catch((err) => {
    console.log(err);
  });

// Start listening for incoming connections
app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
