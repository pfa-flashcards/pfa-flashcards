# PFA Flaschards

**PFA Flashcards** est une application d'aide à la révision permettant d'utiliser des cartes mémoires numériques pour faciliter et accélérer les apprentissages.
Le présent manuel regroupe les informations nécessaires à l'utilisation et à une hypothétique évolution future du logiciel.

Une version de démonstration est accessible à l'adresse http://pfa-flashcards.herokuapp.com/.


## Installation

Ces instructions vous permettront d'obtenir une copie du projet, puis de l'exécuter sur votre machine pour la mettre à disposition d'utilisateurs ou développer de nouvelles fonctionnalités.

### Prérequis

L'application repose sur l'utilisation de conteneurs [Docker](https://www.docker.com/) pour faciliter la mise en place et la configuration des composants logiciels [NodeJS](https://nodejs.org/fr/) et  [MongoDB](https://www.mongodb.com/).

Avant de mettre l'application en production, vous devriez changer les mots de passe MongoDB dans les fichiers `docker-compose.yml` et `back/config/default.json`.

### Instructions

Pour démarrer le projet, il suffit d'exécuter les commandes suivantes :
```bash
# Installer et démarrer les conteneurs
docker-compose up -d

# Compiler l'application
docker exec -it node npm run build

# Démarrer le serveur web
docker exec -it node npm start
```

## Maintenance

### Outils de développement

Pour le développement, privilégier l'éditeur Visual Studio Code : une configuration est fournie pour la partage de standards de code et l'intégration avec le conteneur de développement. En particulier, un _linter_ est configuré pour formatter le code lors de la sauvegarde.

Au lancement, l'éditeur installe les extensions proposées et demande à démarrer les conteneurs. Les outils se mettent en place automatiquement. En ouvrant un terminal dans VSCode (ou en utilisant `docker exec -it node bash`), vous pouvez lancer les serveurs de développement associés aux répertoires `front` et `back` :

```bash
npm run dev
```

### Conseils pour le développement d'une fonctionnalité

Voici les principales étapes à suivre pour l'ajout d'une fonctionnalité :
- Modifier les modèles de la base de données (`back/models`) si nécessaire.
- Ajouter les fonctions dans le dossier `controllers` du repertoire `back`.
- Mettre en place les routes du serveur (`back/routes`).
- Tester unitairement les fonctions et routes ajoutées (`back/test`).
- Insérer les nouvelles routes dans la documentation de l'API (fichiers `.yml`).
- Ajouter des composants ou modifier les composants existants sur le front-end.

### Tests

Des tests portant sur le back-end sont implémentés. Si vous utilisez la [configuration de développement](#Outils-de-développement), il est possible de les exécuter pour savoir vérifier les fonctionnalités ainsi que la couverture des tests :
```bash
npm run test
```


### Architecture

```bash
.
├── .devcontainer    # Configuration du conteneur de développement
├── .vscode          # Configuration partagée pour l'éditeur VSCode
│ 
├── back             # Code de la partie serveur
│   ├── config
│   │   └── default.json   # Fichier de configuration du serveur
│   ├── models             # Définition des modèles Mongoose
│   ├── controllers        # Fonctions permettant de manipuler des données
│   ├── test               # Tests unitaires sur les contrôleurs
│   ├── routes             # Déclaration des routes du serveur
│   ├── middleware         # Fonctions interceptant les requêtes
│   │   └── auth.js        # Middleware d'authentification des utilisateurs
│   ├── openapi.js         # Configuration de le validation des requêtes
│   ├── main.js            # Création de l'instance Express
│   └── server.js          # Extraction de la configuration et démarrage
|
├── docker-compose.yml     # Fichier de configuration Docker
|
└── front            # Code de la partie client
    ├── build              # Application compilée pour le serveur
    └── src                # Code source React
        ├── actions        # Définition d'états pour l'application
        ├── api            # Fonction pour interagir avec le serveur
        ├── components     # Composants React
        │   ├── auth       # Composants d'authentification et tests
        │   ├── layout     # Composants de fonctionnalité
        │   └── utils      # Composants réutilisables (Dropzone, Modal...)
        ├── constants      # Constantes utilisées dans l'application
        ├── hooks          # Hooks React supplémentaires
        ├── App.jsx        # Fichier principal de l'application
        └── index.js       # Configuration de l'application
```

## Utilisation

Une fois mise en route, l'application est accessible sur le port spécifié dans la configuration (http://localhost:3000/ par défaut). L'application étant pensée pour être intuitive, ces brèves explications visent à présenter rapidement le fonctionnement de l'application.

Pour accéder aux fonctionnalités de l'application, un utilisateur doit disposer d'un compte. Ensuite, il peut créer des decks ou lier des decks publics à son compte en utilisant la recherche.

Au sein d'un deck, un utilisateur peut créer des cartes, les modifier et les supprimer. Il peut ensuite réviser un deck. Durant une révision, l'application enregistre la performance afin d'informer du niveau de connaissance sur ce deck, mais aussi d'améliorer l'efficacité des prochaines revisions en proposant d'abord des cartes non connues.
