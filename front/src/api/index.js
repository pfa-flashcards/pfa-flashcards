const API_ROOT = "/api";

export function fetchAPI(route, options) {
  if (!options.method) options.method = "get";
  if (options.body) options.body = JSON.stringify(options.body);
  options.headers = { ...options.headers, "Content-Type": "application/json" };

  return fetch(`${API_ROOT}/${route}`, options);
}

export function fetchAPIAuth(route, options) {
  return fetchAPI(route, {
    ...options,
    headers: {
      "x-auth-token": localStorage.token || "",
    },
  });
}

export function signIn({ email, password }) {
  return fetchAPI("auth", {
    method: "POST",
    body: { email, password },
  });
}

export function signUp(formData) {
  return fetchAPI("users", {
    method: "POST",
    body: formData,
  });
}

export function getProfile() {
  return fetchAPIAuth("users/me");
}

export function createDeck(deckName) {
  return fetchAPIAuth("decks", { method: "post", body: { name: deckName } });
}

export function deleteDeck(deckId) {
  return fetchAPIAuth(`decks/${deckId}`, { method: "delete" });
}

export function getDecks() {
  return fetchAPIAuth("decks");
}
export function getDeck(deckId) {
  return fetchAPIAuth(`decks/${deckId}`);
}

export function setDeckPrivacy(deckId, privacy) {
  return fetchAPIAuth(`decks/${deckId}/privacy/${privacy}`, { method: "put" });
}

export function createCard(deckId, card) {
  return fetchAPIAuth("cards", { method: "post", body: { deckId, card } });
}

export function deleteCard(cardId) {
  return fetchAPIAuth(`cards/${cardId}`, { method: "delete" });
}

export function getCards(deckId) {
  return fetchAPIAuth(`decks/${deckId}/cards`);
}

export function getCardsSorted(deckId) {
  return fetchAPIAuth(`decks/${deckId}/cards/sorted`);
}

export function updateCard(cardId, card) {
  return fetchAPIAuth(`cards/${cardId}`, {
    method: "put",
    body: { cardId, card },
  });
}

export function getCard(cardId) {
  return fetchAPIAuth(`cards/${cardId}`, { method: "get" });
}

export function createReport(deckId, score) {
  return fetchAPIAuth("reports", { method: "post", body: { deckId, score } });
}

export function createEval(cardId, score, reportId, reportScore) {
  return fetchAPIAuth("evals", {
    method: "post",
    body: { cardId, score, reportId, reportScore },
  });
}

export function getLastEval(cardId) {
  return fetchAPIAuth(`evals/${cardId}/last`, { method: "get" });
}

export function getLastReport(deckId) {
  return fetchAPIAuth(`reports/deck/${deckId}/last`, { method: "get" });
}

export function searchDeckByPattern(pattern) {
  return fetchAPIAuth(`decks/search/${pattern}`, { method: "get" });
}

export function linkUserToDeck(deckId) {
  return fetchAPIAuth(`decks/${deckId}/link`, { method: "put" });
}

export function getReports(deckId) {
  return fetchAPIAuth(`reports/deck/${deckId}`, { method: "get" });
}
