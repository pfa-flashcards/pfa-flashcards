// Display texts for easinesses
export const easinessTexts = {
  0: "A revoir",
  1: "Difficile",
  2: "Moyen",
  3: "Facile",
};

// Bootstrap variants for visual feedback on easinesses
export const easinessVariants = {
  0: "danger",
  1: "warning",
  2: "info",
  3: "success",
};
