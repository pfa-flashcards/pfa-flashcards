import { useCallback, useRef, useState } from "react";

// inspired from https://github.com/streamich/react-use/

const isTouchEvent = (ev) => {
  return "touches" in ev;
};

const preventDefault = (ev) => {
  if (!isTouchEvent(ev)) return;

  if (ev.touches.length < 2 && ev.preventDefault) {
    ev.preventDefault();
  }
};

const useLongPress = (
  callback,
  { isPreventDefault = true, delay = 400, threshold = 10 } = {}
) => {
  const timeout = useRef();
  const target = useRef();
  const [firstTouch, setFirstTouch] = useState({ x: 0, y: 0 });

  const start = useCallback(
    (event) => {
      // record first touch position
      if (isTouchEvent(event)) {
        setFirstTouch({
          x: event.touches[0].screenX,
          y: event.touches[0].screenY,
        });
      }

      // prevent ghost click on mobile devices
      if (isPreventDefault && event.target) {
        event.target.addEventListener("touchend", preventDefault, {
          passive: false,
        });
        target.current = event.target;
      }
      timeout.current = setTimeout(() => callback(event), delay);
    },
    [callback, delay, isPreventDefault]
  );

  const clear = useCallback(() => {
    // clearTimeout and removeEventListener
    timeout.current && clearTimeout(timeout.current);

    if (isPreventDefault && target.current) {
      target.current.removeEventListener("touchend", preventDefault);
    }
  }, [isPreventDefault]);

  const touchmove = useCallback(
    (e) => {
      if (
        e.targetTouches.length === 0 ||
        Math.abs(e.targetTouches[0].screenX - firstTouch.x) > threshold ||
        Math.abs(e.targetTouches[0].screenY - firstTouch.y) > threshold
      )
        clear();
    },
    [firstTouch, threshold, clear]
  );

  return {
    onTouchStart: (e) => start(e),
    onTouchEnd: clear,
    onTouchMove: (e) => touchmove(e),
  };
};

export default useLongPress;
