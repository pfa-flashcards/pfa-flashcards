import React, { useState } from "react";
import PropTypes from "prop-types";
import { Modal, Button } from "react-bootstrap";
import { Search } from "react-bootstrap-icons";
import DeckSearch from "./DeckSearch";

const SearchModal = (props) => {
  const modalProps = {
    show: props.show,
    onHide: props.onHide,
  };
  return (
    <Modal
      {...modalProps}
      backdrop="static"
      keyboard={false}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Recherche d&apos;un deck par son identifiant
        </Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ height: "350px" }}>
        <DeckSearch
          update={props.update}
          onHide={props.onHide}
          decks={props.decks}
        />
      </Modal.Body>
    </Modal>
  );
};

SearchModal.propTypes = {
  show: PropTypes.bool,
  onHide: PropTypes.func,
  update: PropTypes.func,
  decks: PropTypes.array,
};

const SearchModalButton = (props) => {
  const [modalShow, setModalShow] = useState(false);

  return (
    <>
      <Button variant="primary" onClick={() => setModalShow(true)}>
        <Search /> {" Chercher"}
      </Button>

      <SearchModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        update={props.update}
        decks={props.decks}
      />
    </>
  );
};

SearchModalButton.propTypes = {
  update: PropTypes.func,
  decks: PropTypes.array,
};

export default SearchModalButton;
