import React, { useMemo } from "react";
import { useDropzone } from "react-dropzone";
import PropTypes from "prop-types";

const baseStyle = {
  flex: 1,
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  padding: "20px",
  borderWidth: 2,
  borderRadius: 2,
  borderColor: "#eeeeee",
  borderStyle: "dashed",
  backgroundColor: "#fafafa",
  color: "#bdbdbd",
  outline: "none",
  transition: "border .24s ease-in-out",
};

const activeStyle = {
  borderColor: "#2196f3",
};

const acceptStyle = {
  borderColor: "#00e676",
};

const rejectStyle = {
  borderColor: "#ff1744",
};

function DropZone(props) {
  const onDrop = (acceptedFiles) => {
    if (acceptedFiles.length) {
      props.setDrop((prev) => [...prev, ...acceptedFiles]);
    }
  };

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject,
  } = useDropzone({ accept: props.accepted, onDrop });

  const style = useMemo(
    () => ({
      ...baseStyle,
      ...(isDragActive ? activeStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {}),
    }),
    [isDragActive, isDragReject, isDragAccept]
  );

  const files = props.drop.map((file) => <li key={file.path}>{file.path}</li>);

  return (
    <div>
      <div {...getRootProps({ style })}>
        <input {...getInputProps()} />
        {props.children}
      </div>
      <ul>{files}</ul>
    </div>
  );
}

DropZone.propTypes = {
  setDrop: PropTypes.func,
  drop: PropTypes.array,
  accepted: PropTypes.string,
  children: PropTypes.element,
};

export default DropZone;
