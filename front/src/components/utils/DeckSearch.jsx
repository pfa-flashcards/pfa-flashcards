import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Form, FormControl } from "react-bootstrap";

import { searchDeckByPattern, linkUserToDeck } from "../../api";
import { Row, Card, Button, OverlayTrigger, Tooltip } from "react-bootstrap";
import { PersonCircle, PlusSquare, Eye } from "react-bootstrap-icons";

const DeckAction = (props) => {
  return (
    <div
      className="d-flex flex-column"
      style={{ position: "absolute", top: 5, right: 2 }}
    >
      <OverlayTrigger
        placement="left"
        delay={{ show: 250 }}
        overlay={<Tooltip>Dupliquer</Tooltip>}
      >
        <Button variant="link" onClick={() => props.duplicateDeck(props.id)}>
          <PlusSquare />
        </Button>
      </OverlayTrigger>
    </div>
  );
};

DeckAction.propTypes = {
  duplicateDeck: PropTypes.func,
  id: PropTypes.string,
};

const DeckInfo = (props) => {
  return (
    <Card style={{ width: "90em" }}>
      <Card.Header>
        {props.name}
        <DeckAction
          id={props.id}
          duplicateDeck={props.duplicateDeck}
        ></DeckAction>
      </Card.Header>
      <Card.Body>
        <div className="d-flex justify-content-between">
          <div className="p-2">
            <PersonCircle /> {props.owner.firstname} {props.owner.lastname}{" "}
          </div>
          <div className="p-2">
            <Eye /> {props.numberAuthorized}
          </div>
          <div className="p-2">Nombre de Carte: {props.numberCards}</div>
        </div>
      </Card.Body>
    </Card>
  );
};

DeckInfo.propTypes = {
  name: PropTypes.string,
  owner: PropTypes.object,
  id: PropTypes.string,
  numberCards: PropTypes.number,
  numberAuthorized: PropTypes.number,
  duplicateDeck: PropTypes.func,
};

const DeckSearch = (props) => {
  const [searchTerm, setSearchTerm] = useState("");
  const [data, setData] = useState({});
  const [userDecks, setUserDecks] = useState({});

  const getDecksInfo = async () => {
    if (searchTerm) {
      let res = await searchDeckByPattern(searchTerm);
      res = await res.json();
      setData(res);
    } else {
      setData([]);
    }
  };

  const duplicateDeck = async (deckId) => {
    setData([]);
    setSearchTerm("");
    await linkUserToDeck(deckId);
    props.update();
    props.onHide();
  };

  const handleSearchTerm = (e) => {
    e.preventDefault();
    setSearchTerm(e.target.value);
  };

  const onKeyPress = (e) => {
    if (e.charCode === 13) {
      e.preventDefault();
      return false;
    }
  };

  useEffect(() => {
    getDecksInfo();
  }, [searchTerm]);

  useEffect(() => {
    setUserDecks(
      props.decks.map((deck) => {
        return deck._id;
      })
    );
  }, []);

  return (
    <>
      <Form>
        <FormControl
          type="text"
          placeholder="Entrer le nom du deck recherché"
          onChange={handleSearchTerm}
          onKeyPress={onKeyPress}
        ></FormControl>
      </Form>
      {data.length ? (
        <div
          className="mt-2"
          style={{ height: 280, overflowY: scroll, overflowX: "hidden" }}
        >
          {data.map((data) => {
            if (!userDecks.includes(data._id)) {
              return (
                <Row className="mt-2 pl-4 pr-4" key={data._id}>
                  <DeckInfo
                    id={data._id}
                    name={data.name}
                    owner={data.owner}
                    numberCards={data.nbCards}
                    numberAuthorized={data.nbAuthorized}
                    duplicateDeck={duplicateDeck}
                  ></DeckInfo>
                </Row>
              );
            }
          })}
        </div>
      ) : (
        <p></p>
      )}
    </>
  );
};

DeckSearch.propTypes = {
  onHide: PropTypes.func,
  update: PropTypes.func,
  decks: PropTypes.array,
};

export default DeckSearch;
