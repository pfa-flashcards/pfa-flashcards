import React from "react";
import PropTypes from "prop-types";
import Form from "react-bootstrap/Form";

const TextInput = (props) => {
  return (
    <Form.Group>
      <Form.Label htmlFor={props.name}>{props.label}</Form.Label>
      <Form.Control
        type={props.type || "text"}
        value={props.value}
        onChange={props.onChange}
        placeholder={props.placeholder}
        name={props.name}
        id={props.name}
        required={props.required}
        as={props.as || "input"}
      />
    </Form.Group>
  );
};

TextInput.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string,
  required: PropTypes.bool,
  as: PropTypes.string,
};

export default TextInput;
