import React from "react";
import PropTypes from "prop-types";
import {
  ComposedChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
} from "recharts";

export default function ReportChart(props) {
  const height = 400;
  return (
    <div style={{ width: "100%", height: height }}>
      <ResponsiveContainer>
        <ComposedChart
          data={props.data}
          margin={{
            top: 75,
            right: 80,
            left: 20,
            bottom: 50,
          }}
          padding={{ top: 10 }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey={props.xAxis} />
          <YAxis />
          <Tooltip />
          <Bar dataKey={props.valueName} fill="#8884d8" barSize={50} />
        </ComposedChart>
      </ResponsiveContainer>
    </div>
  );
}

ReportChart.propTypes = {
  data: PropTypes.array,
  valueName: PropTypes.string,
  xAxis: PropTypes.string,
  yAxis: PropTypes.string,
};
