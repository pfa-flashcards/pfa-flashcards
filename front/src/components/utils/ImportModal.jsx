import React, { useState } from "react";
import PropTypes from "prop-types";
import { Modal, Button } from "react-bootstrap";
import TextInput from "./TextInput";
import DropZone from "./DropZone";
import Papa from "papaparse";
import { createCard } from "../../api";

function TextImport(props) {
  const onChange = (e) => {
    props.setText(e.target.value);
  };

  return (
    <TextInput
      type="text"
      value={props.text}
      onChange={onChange}
      label=""
      name="TextImport"
      placeholder={`Recto;Verso\nRecto;Verso`}
      required={false}
      as={"textarea"}
    />
  );
}

TextImport.propTypes = {
  text: PropTypes.string,
  setText: PropTypes.func,
};

function arrayToCards(cardsArray) {
  let newCardsArray = [];
  for (let card of cardsArray) {
    if (card.length == 2) {
      newCardsArray.push({ recto: card[0], verso: card[1] });
    }
  }
  return newCardsArray;
}

function ImportGroup(props) {
  const [text, setText] = useState("");
  const [drop, setDrop] = useState([]);

  async function onClick() {
    let textCards;
    let filesCards = [];

    const textParsed = new Promise((resolve, reject) => {
      Papa.parse(text, {
        delimiter: ";",
        complete: function (results) {
          textCards = arrayToCards(results.data);
          resolve();
        },
        error: () => reject(),
      });
    });

    const filesParsed = drop.map((file) => {
      return new Promise((resolve, reject) => {
        Papa.parse(file, {
          delimiter: ";",
          complete: function (results) {
            filesCards = filesCards.concat(arrayToCards(results.data));
            resolve();
          },
          error: () => reject(),
        });
      });
    });

    const allParsed = filesParsed.concat(textParsed);
    Promise.all(allParsed).then(() => {
      const allCards = textCards.concat(filesCards);
      createCard(props.deckId, allCards).then(() => {
        props.update();
        props.onHide();
      });
    });

    setText("");
    setDrop([]);
  }

  return (
    <div>
      <TextImport text={text} setText={setText} />
      <DropZone
        drop={drop}
        setDrop={setDrop}
        accepted="text/plain, application/vnd.ms-excel"
      >
        <p style={{ margin: "auto" }}>
          Glisser et déposer un fichier, ou cliquer pour sélectionner un fichier
        </p>
      </DropZone>
      <Button variant={"outline-primary"} onClick={onClick}>
        Tout importer
      </Button>
    </div>
  );
}

ImportGroup.propTypes = {
  deckId: PropTypes.string,
  update: PropTypes.func,
  onHide: PropTypes.func,
};

function ImportModal(props) {
  const modalProps = { show: props.show, onHide: props.onHide };
  return (
    <Modal
      {...modalProps}
      backdrop="static"
      keyboard={false}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Importation
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ImportGroup
          update={props.update}
          onHide={props.onHide}
          deckId={props.deckId}
        />
      </Modal.Body>
    </Modal>
  );
}

ImportModal.propTypes = {
  show: PropTypes.bool,
  onHide: PropTypes.func,
  deckId: PropTypes.string,
  update: PropTypes.func,
};

function ImportModalButton(props) {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <>
      <Button variant="primary" onClick={() => setModalShow(true)}>
        Importer
      </Button>

      <ImportModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        update={props.update}
        deckId={props.deckId}
      />
    </>
  );
}

ImportModalButton.propTypes = {
  deckId: PropTypes.string,
  update: PropTypes.func,
};

export default ImportModalButton;
