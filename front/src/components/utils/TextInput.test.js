import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import TextInput from "./TextInput";

describe("TextInput component", () => {
  const State = { value: "" };
  const onChange = (e) => {
    State.value = e.target.value;
  };

  beforeEach(() => {
    State.value = "";
    render(
      <TextInput
        value={State.value}
        onChange={onChange}
        name="inputname"
        label="Input label"
        placeholder="Input placeholder"
        type="text"
        required
      />
    );
  });

  test("should display the placeholder", () => {
    expect(
      screen.getByPlaceholderText("Input placeholder")
    ).toBeInTheDocument();
  });

  test("should be associated to a label", () => {
    expect(screen.getByLabelText("Input label")).toBeInTheDocument();
  });

  test("should update the value with what the user typed", () => {
    const input = screen.getByLabelText("Input label");

    fireEvent.change(input, {
      target: { value: "azerty" },
    });
    expect(State.value).toBe("azerty");
  });
});
