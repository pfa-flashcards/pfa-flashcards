import React, { createContext, useReducer, useEffect } from "react";
import PropTypes from "prop-types";

import { loadUser } from "../../actions/auth";

import {
  IN_FLIGHT_REQUEST,
  REGISTER_SUCCESS,
  REGISTER_FAILED,
  AUTH_ERROR,
  USER_LOADED,
  LOGIN_FAILED,
  LOGIN_SUCCESS,
  LOGOUT,
} from "../../actions/types";

export const AuthContext = createContext();

const authInitialState = {
  isAuthenticated: false,
  loading: true,
};

/**
 * A reducer for the authentication context
 * @param {Object} state the previous state
 * @param {Object} action the action to apply
 */
function authReducer(state, action) {
  switch (action.type) {
    case IN_FLIGHT_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case USER_LOADED:
      return {
        ...state,
        ...action.payload,
        isAuthenticated: true,
        loading: false,
      };
    case REGISTER_SUCCESS:
    case LOGIN_SUCCESS:
      localStorage.setItem("token", action.payload.token);
      return {
        ...state,
        ...action.payload,
        isAuthenticated: true,
        loading: true,
      };
    case REGISTER_FAILED:
    case LOGIN_FAILED:
    case AUTH_ERROR:
    case LOGOUT:
      localStorage.removeItem("token");
      return {
        isAuthenticated: false,
        loading: false,
      };
    default:
      return state;
  }
}

export const AuthContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(authReducer, authInitialState);
  useEffect(() => loadUser(dispatch), []);

  return (
    <AuthContext.Provider value={{ ...state, dispatch }}>
      {children}
    </AuthContext.Provider>
  );
};

AuthContextProvider.propTypes = {
  children: PropTypes.element,
};
