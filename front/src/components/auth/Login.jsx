import React, { useState, useContext } from "react";
import { Link, Redirect } from "react-router-dom";
import { Col, Form, Button } from "react-bootstrap";
import { login } from "../../actions/auth";
import { AuthContext } from "./AuthContext";
import TextInput from "../utils/TextInput";

/**
 * The Login component, a form to receive the user email and password
 */
const Login = () => {
  const { isAuthenticated, loading, dispatch } = useContext(AuthContext);

  // Redirect to dashboard if the user is logged in
  if (isAuthenticated) {
    return <Redirect to="/dashboard" />;
  }

  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  const { email, password } = formData;

  const [failure, setFailure] = useState(false);

  const onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
    setFailure(false);
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    await login(dispatch, email, password);
    setFailure(!isAuthenticated);
  };

  return (
    <Col as={Form} md={6} className="mx-auto" onSubmit={onSubmit}>
      <h1 className="large-text-primary">Connexion</h1>
      <p className="lead">
        <i className="fas fa-sign-in-alt"></i> Bon retour !
      </p>
      <TextInput
        value={email}
        onChange={onChange}
        name="email"
        type="email"
        label="Email"
        placeholder="john.doe@mail.com"
        required
      />
      <TextInput
        value={password}
        onChange={onChange}
        name="password"
        label="Mot de passe"
        type="password"
        placeholder="MotDePasse"
        minLength="6"
        required
      />
      <Button variant="primary" type="submit" disabled={loading}>
        Connexion
      </Button>
      <span className="text-danger ml-2">
        {!failure || "Identifiant ou mot de passe incorrect"}
      </span>
      <p>
        {"Pas encore de compte ? "}
        <Link to="/register">Créer un compte</Link>
      </p>
    </Col>
  );
};

export default Login;
