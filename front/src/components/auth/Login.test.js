import React from "react";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import { rest } from "msw";
import { setupServer } from "msw/node";

import { BrowserRouter, Route } from "react-router-dom";
import { AuthContextProvider } from "../auth/AuthContext";
import Login from "./Login";

// Initialize mock API server
const server = setupServer(
  rest.post("/api/auth", (req, res, ctx) => {
    return res(ctx.status(200), ctx.json({ token: "awesome_token" }));
  }),
  rest.get("/api/users/me", (req, res, ctx) => {
    return res(ctx.json({}));
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe("Login component", () => {
  beforeEach(() => {
    render(
      <AuthContextProvider>
        <BrowserRouter>
          <Login />
          <Route path="/dashboard">dashboard</Route>
        </BrowserRouter>
      </AuthContextProvider>
    );
  });

  test("should render input components", () => {
    const expected_labels = [/email/i, /Mot de passe/i];
    expected_labels.map((label) =>
      expect(screen.getByLabelText(label)).toBeInTheDocument()
    );
  });

  test("should accept user input", () => {
    const emailInput = screen.getByLabelText(/email/i);
    const passwordInput = screen.getByLabelText(/Mot de passe/i);
    fireEvent.change(emailInput, { target: { value: "test@mail.fr" } });
    fireEvent.change(passwordInput, { target: { value: "p4ssw0rd" } });

    expect(emailInput.value).toBe("test@mail.fr");
    expect(passwordInput.value).toBe("p4ssw0rd");
  });

  test("should display a warning on failed authentication", async () => {
    server.use(
      // override to return a 400 Server Error
      rest.post("/api/auth", (req, res, ctx) => {
        return res(ctx.status(400), ctx.json({}));
      })
    );

    const emailInput = screen.getByLabelText(/email/i);
    const passwordInput = screen.getByLabelText(/Mot de passe/i);

    fireEvent.change(emailInput, { target: { value: "test@mail.fr" } });
    fireEvent.change(passwordInput, { target: { value: "wrongpassword" } });

    fireEvent.click(screen.getByRole("button", { name: /Connexion/i }));

    await waitFor(() => {
      expect(
        screen.getByText(/Identifiant ou mot de passe incorrect/i)
      ).toBeInTheDocument();
    });
  });

  test("should redirect to the Dashboard on successful authentication", async () => {
    const emailInput = screen.getByLabelText(/email/i);
    const passwordInput = screen.getByLabelText(/Mot de passe/i);

    fireEvent.change(emailInput, { target: { value: "test@mail.fr" } });
    fireEvent.change(passwordInput, { target: { value: "p4ssw0rd" } });
    fireEvent.click(screen.getByRole("button", { name: /Connexion/i }));

    await waitFor(() => {
      expect(screen.getByText(/dashboard/i)).toBeInTheDocument();
    });
  });
});
