import React from "react";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import { rest } from "msw";
import { setupServer } from "msw/node";

import { BrowserRouter, Route } from "react-router-dom";
import { AuthContextProvider } from "./AuthContext";
import Register from "./Register";

// Initialize mock API server
const server = setupServer(
  rest.post("/api/users", (req, res, ctx) => {
    return res(ctx.status(200), ctx.json({ token: "awesome_token" }));
  }),
  rest.get("/api/users/me", (req, res, ctx) => {
    return res(ctx.json({}));
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

const form_values = [
  ["Prénom", "John"],
  ["Nom", "Doe"],
  ["Email", "john@mail.com"],
  ["Mot de passe", "p4ssw0rd"],
  ["Confirmation du mot de passe", "p4ssw0rd"],
];

describe("Register component", () => {
  beforeEach(() => {
    render(
      <AuthContextProvider>
        <BrowserRouter>
          <Register />
          <Route path="/dashboard">dashboard</Route>
        </BrowserRouter>
      </AuthContextProvider>
    );
  });

  test("should render input components", () => {
    form_values.map(([label]) =>
      expect(screen.getByLabelText(label)).toBeInTheDocument()
    );
  });

  test("should accept user input", () => {
    form_values.map(([label, value]) =>
      fireEvent.change(screen.getByLabelText(label), { target: { value } })
    );

    form_values.map(([label, value]) => {
      expect(screen.getByLabelText(label).value).toBe(value);
    });
  });

  test("should display a message on non-matching passwords", async () => {
    const emailInput = screen.getByLabelText("Mot de passe");
    const passwordInput = screen.getByLabelText("Confirmation du mot de passe");

    fireEvent.change(emailInput, { target: { value: "password" } });
    fireEvent.change(passwordInput, { target: { value: "different" } });

    await waitFor(() => {
      expect(
        screen.getByText(/Les mots de passe ne correspondent pas/i)
      ).toBeInTheDocument();
    });
  });

  test("should redirect to the Dashboard on successful registration", async () => {
    form_values.map(([label, value]) =>
      fireEvent.change(screen.getByLabelText(label), { target: { value } })
    );

    fireEvent.click(screen.getByRole("button", { name: /Créer le compte/i }));

    await waitFor(() => {
      expect(screen.getByText(/dashboard/i)).toBeInTheDocument();
    });

    expect(localStorage.token).toBe("awesome_token");
  });
});
