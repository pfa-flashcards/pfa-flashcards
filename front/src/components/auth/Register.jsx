import React, { useState, useContext } from "react";
import { Link, Redirect } from "react-router-dom";
import { Col, Form, Button } from "react-bootstrap";
import TextInput from "../utils/TextInput";
import { AuthContext } from "./AuthContext";
import { register } from "../../actions/auth";

/**
 * The Register component, a form to receive the future user's details
 */
const Register = () => {
  const { isAuthenticated, dispatch } = useContext(AuthContext);

  if (isAuthenticated) {
    return <Redirect to="/dashboard" />;
  }

  const [formData, setFormData] = useState({
    firstname: "",
    lastname: "",
    email: "",
    password: "",
    password2: "",
  });

  const { firstname, lastname, email, password, password2 } = formData;

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = (e) => {
    e.preventDefault();
    if (password === password2) {
      register(dispatch, { firstname, lastname, email, password });
    }
  };

  return (
    <Col as={Form} md={6} className="mx-auto" onSubmit={onSubmit}>
      <h1 className="large-text-primary">Créer un compte</h1>
      <p className="lead">
        <i className="fas fa-sign-in-alt"></i> Créer un compte
      </p>
      <TextInput
        value={firstname}
        onChange={onChange}
        name="firstname"
        label="Prénom"
        placeholder="John"
        required
      />
      <TextInput
        value={lastname}
        onChange={onChange}
        name="lastname"
        label="Nom"
        placeholder="Doe"
        required
      />
      <TextInput
        value={email}
        onChange={onChange}
        name="email"
        type="email"
        label="Email"
        placeholder="john.doe@mail.com"
        required
      />
      <TextInput
        value={password}
        onChange={onChange}
        name="password"
        type="password"
        label="Mot de passe"
        placeholder="j0hnd03"
        required
      />
      <Form.Group>
        <Form.Label htmlFor="password2">
          Confirmation du mot de passe
        </Form.Label>

        <Form.Control
          type="password"
          className={
            password2
              ? password === password2
                ? "is-valid"
                : "is-invalid"
              : ""
          }
          placeholder="j0hnd03"
          id="password2"
          name="password2"
          minLength="6"
          value={password2}
          onChange={onChange}
          required
        />
        <Form.Control.Feedback type="invalid">
          {"Les mots de passe ne correspondent pas"}
        </Form.Control.Feedback>
      </Form.Group>

      <Button variant="primary" type="submit">
        Créer le compte
      </Button>
      <p>
        Déjà un compte ? <Link to="/login">Se connecter</Link>
      </p>
    </Col>
  );
};

export default Register;
