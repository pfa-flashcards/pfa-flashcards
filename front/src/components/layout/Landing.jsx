import React, { useContext } from "react";
import { Redirect } from "react-router-dom";
import { Jumbotron, Container } from "react-bootstrap";

import { AuthContext } from "../auth/AuthContext";

/**
 * The landing page
 */
const Landing = () => {
  const { isAuthenticated } = useContext(AuthContext);

  if (isAuthenticated) return <Redirect to="/dashboard" />;

  return (
    <Jumbotron className="h-100">
      <Container fluid="md" className="p-0">
        <h1>{"Apprendre n'a jamais été aussi facile !"}</h1>
        <p>{"Un projet de flashcards à l'ENSEIRB-MATMECA"}</p>
      </Container>
    </Jumbotron>
  );
};

export default Landing;
