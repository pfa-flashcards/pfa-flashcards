import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { Navbar, Nav, Container } from "react-bootstrap";

import { AuthContext } from "../auth/AuthContext";
import { logout } from "../../actions/auth";

/**
 * The navigation menu for the header
 */
const Navigation = () => {
  const { isAuthenticated, loading, firstname, dispatch } = useContext(
    AuthContext
  );

  const authLinks = (
    <Nav className="ml-auto">
      <Link onClick={() => logout(dispatch)} className="nav-link" to="/">
        <i className="fas fa-sign-out-alt mr-2"></i>
        Déconnexion ({firstname})
      </Link>
    </Nav>
  );

  const guestLinks = (
    <Nav className="ml-auto">
      <Link to="/register" className="nav-link">
        Créer un compte
      </Link>
      <Link to="/login" className="nav-link">
        Se connecter
      </Link>
    </Nav>
  );

  return (
    <Navbar bg="light" expand="lg" style={{ minHeight: "4em" }}>
      <Container fluid="md" className="p-0">
        <Link to="/dashboard" className="navbar-brand">
          <i className="fas fa-code mr-2"></i>PFA Flashcards
        </Link>
        <Navbar.Toggle />
        <Navbar.Collapse className="text-right">
          {!loading && (isAuthenticated ? authLinks : guestLinks)}
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Navigation;
