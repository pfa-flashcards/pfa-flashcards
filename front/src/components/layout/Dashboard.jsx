import React, { useContext, useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import Jumbotron from "react-bootstrap/Jumbotron";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import PropTypes from "prop-types";
import SeachModalButton from "../utils/SearchModal";
import { AuthContext } from "../auth/AuthContext";
import { createDeck, getDecks, getLastReport, deleteDeck } from "../../api";
import { Trash } from "react-bootstrap-icons";

import {
  Button,
  Container,
  Card,
  ProgressBar,
  Col,
  Row,
} from "react-bootstrap";

const DeckItem = (props) => {
  const progress = props.deck.score ? (props.deck.score * 100).toFixed(0) : 0;
  const [hovered, setHovered] = useState(false);

  let backgroundColor;
  if (hovered) backgroundColor = "#ddd";
  else backgroundColor = "#fff";

  const delDeck = async () => {
    await deleteDeck(props.deck._id);
    props.update();
  };

  const deleteAction = (
    <Button variant="link" onClick={delDeck}>
      <Trash />
    </Button>
  );

  const action = { button: deleteAction, legend: "Supprimer" };

  return (
    <Card
      className="mb-3"
      key={props.deck._id}
      style={{ backgroundColor, height: "90%" }}
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
    >
      {hovered && (
        <div
          className="d-flex flex-column"
          style={{ position: "absolute", top: 5, right: 0 }}
        >
          <OverlayTrigger
            placement="left"
            delay={{ show: 250 }}
            overlay={<Tooltip>{action.legend}</Tooltip>}
            key={action.legend}
          >
            {action.button}
          </OverlayTrigger>
        </div>
      )}
      <Card.Body
        href={`deck/${props.deck._id}`}
        className="px-3 py-4 d-flex flex-column"
        onClick={() =>
          (window.location.href = `http://${location.host}/deck/${props.deck._id}`)
        }
      >
        <Card.Title className="text-center">{props.deck.name}</Card.Title>
        <ProgressBar
          className="mt-auto"
          now={progress}
          label={`${progress}%`}
        />
      </Card.Body>
    </Card>
  );
};

DeckItem.propTypes = {
  deck: PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.string,
    score: PropTypes.number,
  }),
  update: PropTypes.func,
};

const DeckAdd = (props) => {
  const [name, setName] = useState("");

  const onChange = (e) => {
    setName(e.target.value);
  };

  const onSubmit = async () => {
    let res = await createDeck(name);
    if (res.status === 200) {
      props.update();
    }
    setName("");
  };

  const onKeyPress = (e) => {
    if (e.charCode === 13) {
      onSubmit();
    }
  };

  return (
    <InputGroup className="mt-4">
      <FormControl
        placeholder="Nom du deck"
        value={name}
        onChange={onChange}
        onSubmit={onSubmit}
        onKeyPress={onKeyPress}
      />
      <InputGroup.Append>
        <Button variant="outline-secondary" onClick={onSubmit}>
          Créer un deck
        </Button>
      </InputGroup.Append>
    </InputGroup>
  );
};

DeckAdd.propTypes = {
  update: PropTypes.func,
};

/**
 * A simple dashboard, future home of the application
 */
const Dashboard = () => {
  const { isAuthenticated, loading, firstname } = useContext(AuthContext);

  if (!loading && !isAuthenticated) {
    return <Redirect to="/login" />;
  }

  const [decks, setDecks] = useState([]);

  const updateDecks = async () => {
    let res = await getDecks();
    res = await res.json();

    setDecks(res);

    res = await Promise.all(
      res.map(async (deck) => {
        console.log(deck._id);
        const lastReport = await (await getLastReport(deck._id)).json();
        const score = lastReport.length === 0 ? 0 : lastReport[0].score;
        return { ...deck, score };
      })
    );

    setDecks(res);
  };

  useEffect(updateDecks, []);

  return (
    <Jumbotron className="h-100">
      <Container fluid="md" className="p-0">
        <h1>Bonjour {firstname} !</h1>
        <Row>
          <Col>
            <DeckAdd update={updateDecks} />
          </Col>
          <Col md="auto" className="mt-4">
            <SeachModalButton update={updateDecks} decks={decks} />
          </Col>
        </Row>
        {decks === [] ? (
          <p>{"Vous n'avez pas encore de deck"}</p>
        ) : (
          <Row className="mt-5">
            {decks.map((deck) => (
              <Col sm={6} lg={3} key={deck._id}>
                <DeckItem deck={deck} update={updateDecks} />
              </Col>
            ))}
          </Row>
        )}
      </Container>
    </Jumbotron>
  );
};

export default Dashboard;
