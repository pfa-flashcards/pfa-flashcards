import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  Jumbotron,
  Button,
  Row,
  Col,
  ProgressBar,
  Container,
  Navbar,
  OverlayTrigger,
  Tooltip,
} from "react-bootstrap";
import {
  getCards,
  getDeck,
  deleteCard,
  setDeckPrivacy,
  getLastReport,
} from "../../api";
import ImportModalButton from "../utils/ImportModal";

import {
  ArrowLeft,
  CheckSquare,
  Trash,
  Lock,
  Unlock,
} from "react-bootstrap-icons";

import CardAdd from "./CardAdd";
import CardItem from "./CardItem";

/**
 * The main view of a deck
 */
const Deck = (props) => {
  const [cards, setCards] = useState([]);
  const [selectionCount, setSelectionCount] = useState(0);
  const [deckName, setDeckName] = useState([]);
  const [privacy, setPrivacy] = useState([]);
  const [showVerso, setShowVerso] = useState(true);
  const [author, setAuthor] = useState(0);
  const [score, setScore] = useState(0);

  var deckId = props.match.params.id;

  const updateCards = async () => {
    let res = await getCards(deckId);
    res = await res.json();
    setCards(
      res.map((card) => {
        return { ...card, selected: false };
      })
    );

    const lastReport = await (await getLastReport(deckId)).json();
    setScore(
      lastReport.length === 0 ? 0 : (lastReport[0].score * 100).toFixed(0)
    );
  };

  const pullDeckData = async () => {
    let res = await getDeck(deckId);
    res = await res.json();
    setDeckName(res.name);
    setAuthor(res.owner);
    setPrivacy(res.private);
  };

  const selectCard = (id) => {
    setCards((cards) =>
      cards.map((card) => {
        if (card._id === id) {
          setSelectionCount(selectionCount + (card.selected ? -1 : 1));
          return { ...card, selected: !card.selected };
        }
        return card;
      })
    );
  };

  const selectAllCards = (state) => {
    setCards((cards) =>
      cards.map((card) => {
        return { ...card, selected: state };
      })
    );
    setSelectionCount(() => (state ? cards.length : 0));
  };

  const deleteOneCard = (id) => {
    deleteCard(id).then(updateCards);
    setSelectionCount((count) => count - 1);
  };

  const updatePrivacy = async () => {
    await setDeckPrivacy(deckId, privacy ? "public" : "private");
    setPrivacy(!privacy);
  };

  const deleteSelectedCards = () => {
    Promise.all(
      cards.map((card) => {
        if (card.selected) return deleteCard(card._id);
      })
    ).then(updateCards);
    setSelectionCount(0);
  };

  useEffect(pullDeckData, []);
  useEffect(updateCards, []);

  const actions = [
    {
      icon: ArrowLeft,
      legend: "Annuler",
      onClick: () => selectAllCards(false),
    },
    {
      icon: CheckSquare,
      legend: "Sélectionner tout",
      onClick: () => selectAllCards(true),
    },
    {
      icon: Trash,
      legend: "Supprimer les éléments selectionnés",
      onClick: deleteSelectedCards,
    },
  ];

  return (
    <div>
      {selectionCount > 0 && (
        <Navbar bg="primary" expand="lg" fixed="top" style={{ height: "4em" }}>
          <Container fluid="md" className="p-0">
            {actions.map((action, index) => (
              <OverlayTrigger
                placement="bottom"
                delay={{ show: 250 }}
                overlay={<Tooltip>{action.legend}</Tooltip>}
                key={action.legend}
              >
                <Button
                  onClick={action.onClick}
                  className={index === 1 ? "ml-auto" : ""}
                >
                  <action.icon size="1.5em" />
                </Button>
              </OverlayTrigger>
            ))}
          </Container>
        </Navbar>
      )}
      <Jumbotron className="h-100">
        <Container fluid="md" className="p-0">
          <Row>
            <Col className="d-flex">
              <h1 className="mr-4">{deckName}</h1>
              <OverlayTrigger
                placement="right"
                delay={{ show: 250 }}
                overlay={
                  <Tooltip>Rendre {privacy ? "public" : "privé"}</Tooltip>
                }
              >
                <Button
                  className="d-block align-self-start"
                  onClick={() => updatePrivacy()}
                  variant="outline-secondary"
                >
                  {privacy ? <Lock size="1.5em" /> : <Unlock size="1.5em" />}
                </Button>
              </OverlayTrigger>
            </Col>
            <Col>
              <p className="text-right">{cards.length} cartes dans le deck</p>
              <p className="mt-2 text-right">
                Auteur : {author.firstname} {author.lastname}
              </p>
            </Col>
          </Row>
          <ProgressBar className="bg-light" now={score} label={`${score}%`} />
          <Row>
            <Col>
              <CardAdd update={updateCards} deckId={deckId} />
            </Col>
            <Col md="auto" className="mt-4">
              <ImportModalButton
                update={updateCards}
                deckId={props.match.params.id}
              />
            </Col>
          </Row>
          {cards.length != 0 && (
            <Row className="mt-4 justify-content-center">
              <Button href={`/revision/${props.match.params.id}`}>
                Réviser le deck
              </Button>
              <Button
                className="ml-4"
                variant="outline-secondary"
                onClick={() => setShowVerso(!showVerso)}
              >
                {showVerso ? "Cacher" : "Revéler"} verso
              </Button>
            </Row>
          )}
          <Row className="mt-4 align-items-center">
            {cards.map((card) => (
              <Col key={card._id} sm={12} md={6} lg={4} className="my-2">
                <CardItem
                  id={card._id}
                  recto={card.recto}
                  verso={card.verso}
                  select={() => selectCard(card._id)}
                  selected={card.selected}
                  selecting={selectionCount > 0}
                  showVerso={showVerso}
                  delete={() => deleteOneCard(card._id)}
                  deckId={deckId}
                ></CardItem>
              </Col>
            ))}
          </Row>
        </Container>
      </Jumbotron>
    </div>
  );
};

Deck.propTypes = {
  name: PropTypes.string,
  cards: PropTypes.array,
  match: PropTypes.object, // from react-router
};

export default Deck;
