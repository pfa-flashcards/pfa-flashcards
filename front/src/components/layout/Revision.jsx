import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Jumbotron, Row, Col, Spinner, Container } from "react-bootstrap";
import CardReview from "./CardReview";
import RevisionReport from "./RevisionReport";

import { getCardsSorted, createReport, createEval } from "../../api";

const Revision = (props) => {
  const [cards, setCards] = useState([]);
  const [currentCardIndex, setCurrentCardIndex] = useState(0);
  const currentCard = cards[currentCardIndex];
  const [report, setReport] = useState({});

  const deckId = props.match.params.id;

  const updateCards = async () => {
    let res = await getCardsSorted(deckId);
    res = await res.json();
    setCards(res);
  };

  const updateReport = async () => {
    let rep = await createReport(deckId, "0");
    rep = await rep.json();
    rep.sum = 0;
    setReport(rep);
  };

  const updateEval = (val) => {
    const newSum = report.sum + Number(val);
    const newScore = newSum / (3 * (currentCardIndex + 1));
    setReport((rep) => {
      return {
        ...rep,
        sum: newSum,
        score: newScore,
      };
    });
    setCards((cards) => {
      currentCard.eval = Number(val);
      cards[currentCardIndex] = currentCard;
      return cards;
    });
    createEval(cards[currentCardIndex]._id, Number(val), report._id, newScore);
  };

  useEffect(updateCards, []);
  useEffect(updateReport, []);

  return (
    <Jumbotron className="h-100">
      <Container fluid="md" className="p-0">
        <Row>
          <Col>
            <h1>Session de revision</h1>
          </Col>
        </Row>
        <Row
          md={{ size: "auto", offset: 1 }}
          className="mt-4 justify-content-center"
        >
          {cards.length === 0 ? (
            <Spinner animation="border" role="status">
              <span className="sr-only">Chargement...</span>
            </Spinner>
          ) : currentCardIndex === cards.length ? (
            <Col>
              <RevisionReport
                deckId={deckId}
                cards={cards}
                score={(report.score * 100).toFixed(2)}
              />
            </Col>
          ) : (
            <CardReview
              recto={currentCard.recto}
              verso={currentCard.verso}
              updateCurrent={() => setCurrentCardIndex((value) => value + 1)}
              updateEval={updateEval}
            ></CardReview>
          )}
        </Row>
      </Container>
    </Jumbotron>
  );
};

Revision.propTypes = {
  match: PropTypes.object,
};

export default Revision;
