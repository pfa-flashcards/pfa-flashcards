import React, { useState } from "react";
import PropTypes from "prop-types";
import { Card as BCard, Button, ButtonGroup } from "react-bootstrap";
import { easinessTexts } from "../../constants/revision";

const CardReview = (props) => {
  const [showAnswer, setShowAnswer] = useState(false);

  const sendEval = (val) => {
    props.updateEval(val.target.value);
    setShowAnswer(false);
    props.updateCurrent();
  };

  const feedbackButtons = Object.entries(easinessTexts)
    .reverse()
    .map(([value, text]) => (
      <Button
        className="rounded-pill mx-1"
        onClick={sendEval}
        key={value}
        value={value}
      >
        {text}
      </Button>
    ));

  return (
    <BCard className="text-center" style={{ width: "25rem", height: "20rem" }}>
      {showAnswer && <BCard.Header>{props.recto}</BCard.Header>}
      <BCard.Body className="d-flex align-items-center justify-content-center">
        <BCard.Text>{showAnswer ? props.verso : props.recto}</BCard.Text>
      </BCard.Body>
      <BCard.Footer>
        {showAnswer ? (
          <ButtonGroup>{feedbackButtons}</ButtonGroup>
        ) : (
          <Button variant="primary" onClick={() => setShowAnswer(true)}>
            Afficher la réponse
          </Button>
        )}
      </BCard.Footer>
    </BCard>
  );
};

CardReview.propTypes = {
  recto: PropTypes.string,
  verso: PropTypes.string,
  updateCurrent: PropTypes.func,
  updateEval: PropTypes.func,
};

export default CardReview;
