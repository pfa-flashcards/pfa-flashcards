import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  Jumbotron,
  Form,
  FormControl,
  Button,
  Row,
  Col,
  Container,
} from "react-bootstrap";
import { getCard, updateCard } from "../../api";

const Editer = (props) => {
  const [card, setCard] = useState({ recto: "", verso: "" });

  const [hasChanges, setHasChanges] = useState(false);

  const [initialCard, setInitialCard] = useState({ recto: "", verso: "" });

  const update = async () => {
    let res = await getCard(props.cardId);
    res = await res.json();
    setCard(res);
    setInitialCard(res);
  };

  const onChange = (e) => {
    const updatedCard = { ...card, [e.target.name]: e.target.value };
    setCard(updatedCard);
    setHasChanges(
      initialCard.recto !== updatedCard.recto ||
        initialCard.verso !== updatedCard.verso
    );
  };

  const onSubmitSave = async () => {
    let res = await updateCard(props.cardId, card);
    if (res.status === 200) {
      props.goBack();
    }
  };

  const onSubmitDiscard = async () => {
    setCard(initialCard);
    setHasChanges(false);
  };

  useEffect(update, []);

  return (
    <Form.Group>
      <Form.Label className="mt-3">Recto</Form.Label>
      <FormControl
        placeholder="Recto"
        value={card.recto}
        name="recto"
        onChange={onChange}
        as="textarea"
        rows={4}
        className="mb-4"
      />
      <Form.Label className="mt-1">Verso</Form.Label>
      <FormControl
        placeholder="Verso"
        value={card.verso}
        name="verso"
        onChange={onChange}
        as="textarea"
        rows={4}
        className="mb-4"
      />
      <div className="text-center">
        <Button
          variant="info"
          onClick={hasChanges ? onSubmitSave : props.goBack}
          className="mt-1 mr-2 ml-2"
        >
          {hasChanges
            ? "Sauvegarder et retourner au deck"
            : "Retourner au deck"}
        </Button>
        <Button
          variant="danger"
          onClick={onSubmitDiscard}
          disabled={!hasChanges}
          className="mt-1 mr-2 ml-2"
        >
          {hasChanges ? "Supprimer les changements" : "Aucun changement"}
        </Button>
      </div>
    </Form.Group>
  );
};

Editer.propTypes = {
  cardId: PropTypes.string,
  goBack: PropTypes.func,
};

const CardEdit = (props) => {
  const goBack = () => {
    window.location.href = `http://${location.host}/deck/${props.match.params.deckId}`;
  };

  return (
    <Jumbotron className="h-100">
      <Container className="container-fluid p-0">
        <Row>
          <Col>
            <h1>Edition de carte</h1>
          </Col>
        </Row>
        <Row>
          <Col>
            <Editer cardId={props.match.params.cardId} goBack={goBack} />
          </Col>
        </Row>
      </Container>
    </Jumbotron>
  );
};

CardEdit.propTypes = {
  deckId: PropTypes.string,
  match: PropTypes.object, // from react-router
};

export default CardEdit;
