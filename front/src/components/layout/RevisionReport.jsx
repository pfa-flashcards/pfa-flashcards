import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Row, Col, Button, Badge, ProgressBar } from "react-bootstrap";
import { Link } from "react-router-dom";
import { easinessVariants, easinessTexts } from "../../constants/revision";
import { getReports } from "../../api/index";
import ReportChart from "../utils/ReportChart";

const CardEval = (props) => {
  return (
    <Row className="mt-4 bg-light justify-content-md-center">
      <Col md={10}>{props.recto}</Col>
      <Col md={2}>
        <Badge variant={easinessVariants[props.eval]}>
          {easinessTexts[props.eval]}
        </Badge>
      </Col>
    </Row>
  );
};

CardEval.propTypes = {
  id: PropTypes.string,
  recto: PropTypes.string,
  eval: PropTypes.number,
};

const RevisionReport = (props) => {
  const [reports, setReports] = useState([]);

  const getOldReports = async () => {
    let res = await getReports(props.deckId);
    res = await res.json();
    console.log(res);
    res = res.map((element) => {
      let date = new Date(element.date);
      return {
        ...element,
        date: date.toLocaleDateString(),
      };
    });
    setReports(res);
  };

  useEffect(getOldReports, []);

  return (
    <>
      <p className="text-center mt-3">Score : {props.score}%</p>
      <ProgressBar>
        {Object.entries(easinessVariants)
          .reverse()
          .map(([value, variant]) => {
            const count = props.cards.reduce(
              (total, card) => (card.eval == value ? total + 1 : total),
              0
            );
            const percentage = (count / props.cards.length) * 100;
            return (
              <ProgressBar
                key={value}
                variant={variant}
                label={
                  percentage < 10 ? count : `${easinessTexts[value]} (${count})`
                }
                now={percentage}
              />
            );
          })}
      </ProgressBar>
      <Row className="mt-4 justify-content-center">
        <Button as={Link} variant="primary" to={`/deck/${props.deckId}`}>
          Retourner au deck
        </Button>
      </Row>
      <ReportChart data={reports} valueName={"score"} xAxis={"date"} />

      {props.cards.map((card) => (
        <CardEval key={card._id} recto={card.recto} eval={card.eval}></CardEval>
      ))}
    </>
  );
};

RevisionReport.propTypes = {
  deckId: PropTypes.string,
  score: PropTypes.string,
  cards: PropTypes.array,
};

export default RevisionReport;
