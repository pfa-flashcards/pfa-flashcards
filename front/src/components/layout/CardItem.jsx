import React, { useState } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import {
  Card as BCard,
  Button,
  OverlayTrigger,
  Tooltip,
} from "react-bootstrap";
import { Pencil, Trash } from "react-bootstrap-icons";
import useLongPress from "../../hooks/LongPress";

const CardActions = (props) => {
  const editAction = (
    <Button variant="link">
      <Link to={`/deck/${props.deckId}/cards/${props.id}`}>
        <Pencil />
      </Link>
    </Button>
  );

  const deleteAction = (
    <Button variant="link" onClick={props.delete}>
      <Trash />
    </Button>
  );

  const actions = [
    { button: editAction, legend: "Éditer" },
    { button: deleteAction, legend: "Supprimer" },
  ];

  return (
    <div
      className="d-flex flex-column"
      style={{ position: "absolute", top: 5, right: 0 }}
    >
      {actions.map((action) => (
        <OverlayTrigger
          placement="left"
          delay={{ show: 250 }}
          overlay={<Tooltip>{action.legend}</Tooltip>}
          key={action.legend}
        >
          {action.button}
        </OverlayTrigger>
      ))}
    </div>
  );
};

CardActions.propTypes = {
  id: PropTypes.string,
  delete: PropTypes.func,
  deckId: PropTypes.string,
};

const CardItem = (props) => {
  const [hovered, setHovered] = useState(false);

  const longPressEvent = useLongPress(() => {
    props.select();
    setHovered(false);
    console.log("long pressed");
  });
  const pressEvent = props.selecting
    ? { ...longPressEvent, onTouchStart: () => {} }
    : longPressEvent;

  let backgroundColor;
  if (hovered && props.selected) backgroundColor = "#ddf";
  else if (hovered) backgroundColor = "#ddd";
  else if (props.selected) backgroundColor = "#eef";
  else backgroundColor = "#fff";

  return (
    <BCard
      className="shadow-sm"
      style={{ backgroundColor, userSelect: "none" }}
      onMouseOver={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
      onTouchStart={() => setHovered(false)}
      onTouchEnd={() => setHovered(false)}
    >
      {hovered && !props.selecting && (
        <CardActions
          delete={props.delete}
          id={props.id}
          deckId={props.deckId}
        />
      )}
      <BCard.Body
        className={hovered ? "mr-3" : ""}
        onClick={() => {
          props.select();
          console.log("clicked");
        }}
        {...pressEvent}
      >
        <BCard.Title>{props.recto}</BCard.Title>
        {props.showVerso && <BCard.Text>{props.verso}</BCard.Text>}
      </BCard.Body>
    </BCard>
  );
};

CardItem.propTypes = {
  id: PropTypes.string,
  recto: PropTypes.string,
  verso: PropTypes.string,
  showVerso: PropTypes.bool,
  selected: PropTypes.bool,
  select: PropTypes.func,
  selecting: PropTypes.bool,
  delete: PropTypes.func,
  deckId: PropTypes.string,
};

export default CardItem;
