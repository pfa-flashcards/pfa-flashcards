import React from "react";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import { rest } from "msw";
import { setupServer } from "msw/node";

import { BrowserRouter, Route } from "react-router-dom";
import { AuthContextProvider } from "../auth/AuthContext";
import Navigation from "./Navigation";

// Initialize mock API server
const server = setupServer(
  rest.get("/api/users/me", (req, res, ctx) => {
    return res(ctx.status(200), ctx.json({ firstname: "John" }));
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

const renderTest = () =>
  render(
    <AuthContextProvider>
      <BrowserRouter>
        <Navigation />
        <Route path="/dashboard">dashboard</Route>
      </BrowserRouter>
    </AuthContextProvider>
  );

describe("Navigation component", () => {
  test("should render guest links when not authenticated", () => {
    renderTest();

    const expected_items = [/Se connecter/i, /Créer un compte/i];

    expected_items.map((item) =>
      expect(screen.getByText(item)).toBeInTheDocument()
    );
  });

  test("should render logout links when authenticated", async () => {
    localStorage.token = "mocktoken";
    renderTest();
    await waitFor(() =>
      expect(
        screen.getByRole("link", { name: /Déconnexion/i })
      ).toBeInTheDocument()
    );
  });

  test("should remove token when the users logs out", async () => {
    localStorage.token = "mocktoken";
    renderTest();

    let logOutLink;
    await waitFor(
      () => (logOutLink = screen.getByRole("link", { name: /Déconnexion/i }))
    );

    fireEvent.click(logOutLink);
    expect(localStorage.token).toBeUndefined();
  });
});
