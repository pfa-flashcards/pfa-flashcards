import React, { useState, useRef } from "react";
import PropTypes from "prop-types";
import { InputGroup, FormControl, Button } from "react-bootstrap";
import { createCard } from "../../api";

const CardAdd = (props) => {
  const rectoElement = useRef(null);
  const versoElement = useRef(null);
  const [card, setCard] = useState({ recto: "", verso: "" });

  const onChange = (e) => {
    setCard({ ...card, [e.target.name]: e.target.value });
  };

  const onSubmit = async () => {
    let res = await createCard(props.deckId, card);
    if (res.status === 200) {
      props.update();
    }
    setCard({ recto: "", verso: "" });
  };

  const onKeyPressRecto = (e) => {
    if (e.charCode === 13) {
      versoElement.current.focus();
    }
  };

  const onKeyPressVerso = (e) => {
    if (e.charCode === 13) {
      onSubmit();
      rectoElement.current.focus();
    }
  };

  return (
    <InputGroup className="mt-4">
      <FormControl
        placeholder="Recto"
        value={card.recto}
        name="recto"
        onChange={onChange}
        ref={rectoElement}
        onKeyPress={onKeyPressRecto}
      />
      <FormControl
        placeholder="Verso"
        value={card.verso}
        name="verso"
        onChange={onChange}
        ref={versoElement}
        onKeyPress={onKeyPressVerso}
      />
      <InputGroup.Append>
        <Button onClick={onSubmit}>Créer une carte</Button>
      </InputGroup.Append>
    </InputGroup>
  );
};

CardAdd.propTypes = {
  update: PropTypes.func,
  deckId: PropTypes.string,
};

export default CardAdd;
