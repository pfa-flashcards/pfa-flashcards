import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Navbar from "./components/layout/Navigation";
import Landing from "./components/layout/Landing";
import Dashboard from "./components/layout/Dashboard";
import Login from "./components/auth/Login";
import Register from "./components/auth/Register";
import Deck from "./components/layout/Deck";
import CardEdit from "./components/layout/CardEdit";
import Revision from "./components/layout/Revision";

import { AuthContextProvider } from "./components/auth/AuthContext";

import "bootstrap/dist/css/bootstrap.min.css";

/**
 * The main component to run the application
 */
function App() {
  return (
    <AuthContextProvider>
      <Router>
        <Navbar />
        <Switch>
          <Route exact path="/" component={Landing} />
          <Route path="/register" component={Register} />
          <Route path="/login" component={Login} />
          <Route path="/dashboard" component={Dashboard} />
          <Route path="/deck/:id" exact component={Deck} />
          <Route path="/deck/:deckId/cards/:cardId" component={CardEdit} />
          <Route path="/revision/:id" component={Revision} />
        </Switch>
      </Router>
    </AuthContextProvider>
  );
}

export default App;
