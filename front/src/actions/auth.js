import {
  IN_FLIGHT_REQUEST,
  REGISTER_SUCCESS,
  REGISTER_FAILED,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  USER_LOADED,
  AUTH_ERROR,
  LOGOUT,
} from "./types";

const api = require("../api");

/**
 * Load a user from the locally stored token, if available
 * @param {function} dispatch a reducer for the authentication status
 */
export async function loadUser(dispatch) {
  if (localStorage.token) {
    try {
      let res = await api.getProfile();
      if (res.status === 200) {
        res = await res.json();
        dispatch({
          type: USER_LOADED,
          payload: res,
        });
      } else dispatch({ type: AUTH_ERROR });
    } catch (err) {
      console.log(err);
      dispatch({ type: AUTH_ERROR });
    }
  } else dispatch({ type: AUTH_ERROR });
}

/**
 * Call the API to register a new user
 * @param {function} dispatch : a reducer for the authentication status
 * @param {object} formData the new user details
 */
export async function register(dispatch, formData) {
  let res;
  try {
    res = await api.signUp(formData);
    res = await res.json();
  } catch (err) {
    console.log(err);
    dispatch({
      type: REGISTER_FAILED,
    });
  }

  if (res.token) {
    dispatch({
      type: REGISTER_SUCCESS,
      payload: res,
    });
    loadUser(dispatch);
  } else
    dispatch({
      type: REGISTER_FAILED,
    });
}

/**
 * Call the API to login a user
 * @param {function} dispatch a reducer for the authentication status
 * @param {String} email
 * @param {String} password
 */
export async function login(dispatch, email, password) {
  let res;

  try {
    dispatch({ type: IN_FLIGHT_REQUEST });
    res = await api.signIn({ email, password });
    res = await res.json();
  } catch (err) {
    console.log(err);
    dispatch({ type: LOGIN_FAILED });
  }
  if (res.token) {
    dispatch({ type: LOGIN_SUCCESS, payload: res });
    loadUser(dispatch);
  } else {
    dispatch({ type: LOGIN_FAILED });
  }
}

/**
 * Logout the current user
 * @param {function} dispatch a reducer for the authentication status
 */
export function logout(dispatch) {
  dispatch({ type: LOGOUT });
}
